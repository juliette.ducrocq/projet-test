# Test de recrutement Lengow

Bienvenue dans le test de recrutement de Lengow pour le poste Wordpress.

Dans la 1ère partie de ce fichier, vous aurez tous les élémments pour installer ce test en local.
Dans la seconde partie vous trouverez les différents exercices.

Bonne chance !


# Installation

## Pré-requis

-  [Git](https://git-scm.com/)
- Docker
	- Linux
		-  [Docker Engine](https://docs.docker.com/engine/installation/linux/ubuntulinux/)
		-  [Docker Compose](https://docs.docker.com/compose/install/)
	- MAC
		-  [Docker Toolbox](https://github.com/docker/toolbox/releases)
		-  [Docker-Machine-NFS](https://github.com/adlogix/docker-machine-nfs)

## Récupérer le test

- Cloner Les fichiers en local
`git clone https://marcrenou@bitbucket.org/lengow-dev/wordpress-recruitment-test.git`

- Lancer Docker
`docker-machine start default`

- Puis Sur Mac, vous devez lancer cette commande
`eval $(docker-machine env default)`

- Puis
`docker-compose up -d`

## Lancer le projet

- Obtenir votre adresse IP
`docker-machine ip default`

- Votre projet est accessible à l'url suivante
`http://YOUR_IP:8282`

- Votre Base de données est accessible à l'url suivante
`http://YOUR_IP:8181`

## Importer la Base de données

- Connectez-vous avec les identifiants suivants
`root / root`

- Importez le base de données située dans le dossier "Eléments"
`wpdb.sql`

## Site

- Connectez-vous avec les identifiants suivants
`lengow / lengow`

# Exercices

Voici les différents exercices que nous vous proposons de réaliser.
L'ensemble des images présentes dans ce fichier sont disponibles dans le dossier "Eléments" situé à la racine du projet.

## Exercice SVG

Mettre le logo Lengow "logo.svg" comme logo du site.
Vous trouverez ce logo dans le dossier "Elément" à la racine du projet.

## Exercice ACF

Le plugin ACF est déjà installé.
Sans passer par le back office, ajouter les champs ACF suivants sur l'ensemble des pages (pas les articles) du site :
- Image
- Description
- Caption
- 
## Exercice CSS / BEM

BEM est une méthode de nommage CSS.
Pour en savoir plus : http://getbem.com/introduction/

Pour cet exercice, vous devez styliser le rendu des champs ACF créer précédemment en respectant la méthodologie BEM. Ajoutez votre code CSS à la fin du fichier style.css. Le rendu final doit correspondre à :

![image-acf-bem-css](https://i.imgur.com/121ZhRI.jpg)

## Bug bouton "Search"

Le bouton de recherche est cassé sur l'ensemble du site !
Trouvez pourquoi et restaurer son état initial.

![bug-search-button](https://i.imgur.com/PCylNry.jpg)

## Bouton "Total Posts count"

Ajouter un bouton "Total Posts Count" sur l'ensemble des articles du site comme sur l'exemple ci-dessous.

![total-posts-count](https://i.imgur.com/MbgZyqy.jpg)

## API Rest

En utilisant l'API Rest de Wordpress, affichez le nombre d'articles présents sur votre site au clic sur le bouton "Total Posts Count". Ajoutez votre code dans le fichier global.js.

![api-rest](https://i.imgur.com/bWEU8V4.jpg)
