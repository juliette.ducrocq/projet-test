<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php twentyseventeen_edit_link( get_the_ID() ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			the_content();
		
			$image = get_field('image');
			$size = 'full'; // (thumbnail, medium, large, full or custom size)
			if($image) {
		?>
			<figure class="attachment">
				<div class="attachment__img-container"><?php echo wp_get_attachment_image($image, $size);?></div>
				<figcaption class="attachment__text-container">
					<p class="attachment__text attachment__text--caption">
						<?php echo wp_get_attachment_caption($image);?>
					</p>
					<p class="attachment__text">
						<?php echo get_post($image)->post_content; ?>
					</p>
				</figcaption>
			</figure>
		<?php } 
			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
					'after'  => '</div>',
				)
			);
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
