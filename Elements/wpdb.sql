-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : wordpress_db
-- Généré le : lun. 08 mars 2021 à 09:15
-- Version du serveur :  10.5.9-MariaDB-1:10.5.9+maria~focal
-- Version de PHP : 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `wpdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://192.168.99.100:8282', 'yes'),
(2, 'home', 'http://192.168.99.100:8282', 'yes'),
(3, 'blogname', 'Lengow', 'yes'),
(4, 'blogdescription', 'Test Recrutement', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'marc.renou@lengow.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:90:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:21:\"safe-svg/safe-svg.php\";i:2;s:41:\"wordpress-importer/wordpress-importer.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:57:\"/var/www/html/wp-content/themes/twentyseventeen/style.css\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'twentyseventeen', 'yes'),
(41, 'stylesheet', 'twentyseventeen', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:5:{i:2;a:4:{s:5:\"title\";s:14:\"Retrouvez-nous\";s:4:\"text\";s:189:\"<strong>Adresse</strong>\nAvenue des Champs-Élysées\n75008, Paris\n\n<strong>Heures d’ouverture</strong>\nDu lundi au vendredi : 9h00&ndash;17h00\nLes samedi et dimanche : 11h00&ndash;15h00\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:20:\"À propos de ce site\";s:4:\"text\";s:99:\"C’est peut-être le bon endroit pour vous présenter et votre site ou insérer quelques crédits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:14:\"Retrouvez-nous\";s:4:\"text\";s:213:\"<p><strong>Adresse</strong><br>Avenue des Champs-Élysées<br>75008, Paris</p><p><strong>Heures d’ouverture</strong><br>Du lundi au vendredi&nbsp;: 9h00–17h00<br>Les samedi et dimanche&nbsp;: 11h00–15h00</p>\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:5;a:4:{s:5:\"title\";s:20:\"À propos de ce site\";s:4:\"text\";s:99:\"C’est peut-être le bon endroit pour vous présenter et votre site ou insérer quelques crédits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/Paris', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'fr_FR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:3;a:3:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;s:9:\"show_date\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:6:\"text-2\";i:1;s:6:\"text-5\";}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-3\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:1:{i:0;s:6:\"text-3\";}s:9:\"sidebar-3\";a:1:{i:0;s:6:\"text-4\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1615195657;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1615206457;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1615206462;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1615214455;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:4:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540307658;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"nav_menu_locations\";a:2:{s:3:\"top\";i:177;s:6:\"social\";i:0;}s:11:\"custom_logo\";i:1791;}', 'yes'),
(126, 'can_compress_scripts', '0', 'no'),
(143, 'recently_activated', 'a:0:{}', 'yes'),
(155, 'acf_version', '5.7.7', 'yes'),
(158, 'new_admin_email', 'marc.renou@lengow.com', 'yes'),
(165, 'current_theme', 'Twenty Seventeen', 'yes'),
(166, 'theme_mods_lengow', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:177;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540308042;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(167, 'theme_switched', '', 'yes'),
(180, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(198, 'theme_mods_content', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:177;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540308197;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:15:\"sidebar_primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:23:\"footer_widget_area_left\";a:0:{}s:25:\"footer_widget_area_center\";a:0:{}s:24:\"footer_widget_area_right\";a:0:{}s:11:\"woocommerce\";a:0:{}s:24:\"wdl_contact_page_sidebar\";a:0:{}}}}', 'yes'),
(200, 'theme_mods_hestia', 'a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:177;}s:29:\"hestia_contact_form_shortcode\";s:14:\"[pirate_forms]\";s:28:\"zerif_frontpage_was_imported\";s:9:\"not-zerif\";s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540387484;s:4:\"data\";a:10:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:17:\"subscribe-widgets\";a:0:{}s:19:\"sidebar-woocommerce\";a:0:{}s:15:\"sidebar-top-bar\";a:0:{}s:14:\"header-sidebar\";a:0:{}s:17:\"sidebar-big-title\";a:0:{}s:18:\"footer-one-widgets\";a:0:{}s:18:\"footer-two-widgets\";a:0:{}s:20:\"footer-three-widgets\";a:0:{}}}s:11:\"custom_logo\";s:0:\"\";}', 'yes'),
(201, 'hestia_contact_form_legacy', '1', 'yes'),
(202, 'hestia_install', '1540308090', 'yes'),
(205, 'hestia_time_activated', '1540368258', 'yes'),
(206, 'hestia_had_elementor', 'no', 'yes'),
(209, 'theme_mods_spicepress', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:177;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540308210;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:15:\"sidebar_primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:23:\"footer_widget_area_left\";a:0:{}s:25:\"footer_widget_area_center\";a:0:{}s:24:\"footer_widget_area_right\";a:0:{}s:11:\"woocommerce\";a:0:{}s:24:\"wdl_contact_page_sidebar\";a:0:{}}}}', 'yes'),
(234, 'hestia_required_actions', 'a:3:{s:19:\"themeisle-companion\";b:1;s:12:\"wpforms-lite\";b:0;s:9:\"elementor\";b:1;}', 'yes'),
(239, 'theme_mods_hestia-child', 'a:6:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:177;}s:28:\"zerif_frontpage_was_imported\";s:9:\"not-zerif\";s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";s:0:\"\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540368257;s:4:\"data\";a:10:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:17:\"subscribe-widgets\";a:0:{}s:19:\"sidebar-woocommerce\";a:0:{}s:15:\"sidebar-top-bar\";a:0:{}s:14:\"header-sidebar\";a:0:{}s:17:\"sidebar-big-title\";a:0:{}s:18:\"footer-one-widgets\";a:0:{}s:18:\"footer-two-widgets\";a:0:{}s:20:\"footer-three-widgets\";a:0:{}}}}', 'yes'),
(242, 'dismissed-hestia_info_obfx', '1', 'yes'),
(279, '_transient_timeout_hestia-update-response', '3080771586', 'no'),
(280, '_transient_hestia-update-response', 'O:8:\"stdClass\":1:{s:11:\"new_version\";s:0:\"\";}', 'no'),
(283, '_transient_ti_sdk_pause_hestia', '1', 'yes'),
(431, 'category_children', 'a:0:{}', 'yes'),
(478, '_site_transient_timeout_browser_430b15e5ae0e6691671ba0e0314e199c', '1615634380', 'no'),
(479, '_site_transient_browser_430b15e5ae0e6691671ba0e0314e199c', 'a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:6:\"14.0.3\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.apple.com/safari/\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/safari.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/safari.png?1\";s:15:\"current_version\";s:2:\"11\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(492, '_transient_timeout_plugin_slugs', '1615126738', 'no'),
(493, '_transient_plugin_slugs', 'a:3:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:21:\"safe-svg/safe-svg.php\";i:2;s:41:\"wordpress-importer/wordpress-importer.php\";}', 'no'),
(494, 'ftp_credentials', 'a:3:{s:8:\"hostname\";s:9:\"localhost\";s:8:\"username\";s:4:\"root\";s:15:\"connection_type\";s:3:\"ftp\";}', 'yes'),
(499, '_transient_is_multi_author', '0', 'yes'),
(509, '_transient_twentyseventeen_categories', '1', 'yes'),
(547, '_site_transient_timeout_theme_roots', '1615196637', 'no'),
(548, '_site_transient_theme_roots', 'a:1:{s:15:\"twentyseventeen\";s:7:\"/themes\";}', 'no'),
(551, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:13:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.6.1\";s:7:\"version\";s:5:\"5.6.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.6.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.6-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.6-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.6\";s:7:\"version\";s:3:\"5.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.3\";s:7:\"version\";s:5:\"5.5.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:6;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.2\";s:7:\"version\";s:5:\"5.5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:7;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.4\";s:7:\"version\";s:5:\"5.4.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:8;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.6-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.6\";s:7:\"version\";s:5:\"5.3.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:9;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.9.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.9.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.9-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.9-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.9\";s:7:\"version\";s:5:\"5.2.9\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:10;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.8\";s:7:\"version\";s:5:\"5.1.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:11;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:60:\"https://downloads.wordpress.org/release/wordpress-5.0.11.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:60:\"https://downloads.wordpress.org/release/wordpress-5.0.11.zip\";s:10:\"no_content\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.11-no-content.zip\";s:11:\"new_bundled\";s:72:\"https://downloads.wordpress.org/release/wordpress-5.0.11-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:6:\"5.0.11\";s:7:\"version\";s:6:\"5.0.11\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:12;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:60:\"https://downloads.wordpress.org/release/wordpress-4.9.16.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:60:\"https://downloads.wordpress.org/release/wordpress-4.9.16.zip\";s:10:\"no_content\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.16-no-content.zip\";s:11:\"new_bundled\";s:72:\"https://downloads.wordpress.org/release/wordpress-4.9.16-new-bundled.zip\";s:7:\"partial\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.16-partial-8.zip\";s:8:\"rollback\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.16-rollback-8.zip\";}s:7:\"current\";s:6:\"4.9.16\";s:7:\"version\";s:6:\"4.9.16\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:5:\"4.9.8\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1615194842;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-10-28 16:02:42\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fr_FR.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(552, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1615194843;s:7:\"checked\";a:1:{s:15:\"twentyseventeen\";s:3:\"1.7\";}s:8:\"response\";a:1:{s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.5\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(553, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1615194844;s:7:\"checked\";a:3:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.7.7\";s:21:\"safe-svg/safe-svg.php\";s:5:\"1.9.9\";s:41:\"wordpress-importer/wordpress-importer.php\";s:5:\"0.6.4\";}s:8:\"response\";a:2:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.6.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:3:\"0.7\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.7.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wordpress-importer/assets/icon-256x256.png?rev=1908375\";s:2:\"1x\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";s:3:\"svg\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:18:\"wordpress-importer\";s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"0.6.4\";s:7:\"updated\";s:19:\"2019-02-07 14:26:55\";s:7:\"package\";s:85:\"https://downloads.wordpress.org/translation/plugin/wordpress-importer/0.6.4/fr_FR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:1:{s:21:\"safe-svg/safe-svg.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/safe-svg\";s:4:\"slug\";s:8:\"safe-svg\";s:6:\"plugin\";s:21:\"safe-svg/safe-svg.php\";s:11:\"new_version\";s:5:\"1.9.9\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/safe-svg/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/safe-svg.1.9.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:61:\"https://ps.w.org/safe-svg/assets/icon-256x256.png?rev=1706191\";s:2:\"1x\";s:53:\"https://ps.w.org/safe-svg/assets/icon.svg?rev=1706191\";s:3:\"svg\";s:53:\"https://ps.w.org/safe-svg/assets/icon.svg?rev=1706191\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/safe-svg/assets/banner-1544x500.png?rev=1706191\";s:2:\"1x\";s:63:\"https://ps.w.org/safe-svg/assets/banner-772x250.png?rev=1706191\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(554, '_transient_timeout_feed_3ca2a73478cc83bbe37e39039b345a78', '1615238076', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(555, '_transient_feed_3ca2a73478cc83bbe37e39039b345a78', 'a:4:{s:5:\"child\";a:1:{s:0:\"\";a:1:{s:3:\"rss\";a:1:{i:0;a:6:{s:4:\"data\";s:3:\"\n\n\n\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:7:\"version\";s:3:\"2.0\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:1:{s:0:\"\";a:1:{s:7:\"channel\";a:1:{i:0;a:6:{s:4:\"data\";s:49:\"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:6:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:4:\"WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"https://wpfr.net\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Site officiel de la communauté\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:13:\"lastBuildDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 23 Dec 2020 11:22:32 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"language\";a:1:{i:0;a:5:{s:4:\"data\";s:5:\"fr-FR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"item\";a:10:{i:0;a:6:{s:4:\"data\";s:57:\"\n		\n		\n		\n		\n		\n				\n		\n\n					\n										\n					\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:4:{s:0:\"\";a:6:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:36:\"Site hacké ou piraté : que faire ?\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/W2RcL0iquis/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 23 Dec 2020 11:22:31 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:10:\"Sécurité\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2363666\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:525:\"Sur le forum WPFR, l&#8217;équipe de modération est souvent interpellée pour des problèmes liés au piratage de site. Parfois, cela commence tout simplement par un site qui ne &#8220;répond&#8221; plus, ou une connexion impossible. WordPress lui-même n&#8217;est pas en cause, souvent il s&#8217;agit d&#8217;une maintenance insuffisante de votre part … ou d&#8217;un pirate qui a<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Flobogo\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:14285:\"<p>Sur le forum WPFR, l&#8217;équipe de modération est souvent interpellée pour des problèmes liés au piratage de site. Parfois, cela commence tout simplement par un site qui ne &#8220;répond&#8221; plus, ou une connexion impossible. WordPress lui-même n&#8217;est pas en cause, souvent il s&#8217;agit d&#8217;une maintenance insuffisante de votre part … ou d&#8217;un pirate qui a pris la main sur le site ! <img src=\"https://s.w.org/images/core/emoji/13.0.1/72x72/1f631.png\" alt=\"😱\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></p>\n<p>C&#8217;est inquiétant, mais pas irréparable. Nous verrons d&#8217;abord les signes qui doivent vous alerter, puis comment nettoyer et réparer le site … et si possible, éviter que cela se reproduise.</p>\n<h2 id=\"les-signes-dalerte\">Les signes d&#8217;alerte</h2>\n<p>Voici les signes qui doivent vous alerter :</p>\n<ul>\n<li>Votre mot de passe n&#8217;est pas reconnu et vous n&#8217;avez plus accès à l&#8217;administration de votre site<br />\nVérifiez tout de même que ce n&#8217;est pas une simple erreur ou oubli de votre mot de passe, que vous pouvez résoudre par une récupération via votre adresse mail ou éventuellement par une modification dans la base de données. (<a href=\"https://wpchannel.com/wordpress/tutoriels-wordpress/remise-zero-mot-passe-wordpress/\">tuto sur WPChannel</a>)</li>\n<li>Une image ou du contenu que vous n&#8217;avez pas inséré vous-même apparaît sur le site<br />\nSi vous n&#8217;êtes pas seul·e à administrer ou éditer le site, vérifiez si une autre personne identifiée n&#8217;a pas rajouté ce contenu.</li>\n<li>Votre site envoie des messages de spam à tous vos abonné·es</li>\n<li>Votre site comporte des liens non voulus ou renvoie directement vos visiteurs vers un site commercial ou porno</li>\n</ul>\n<p>Si votre site n&#8217;est pas sécurisé en HTTPS (votre adresse URL commence par <strong>http:</strong>// ), les risques de piratage sont plus élevés.<br />\nDe même si votre version de WordPress n&#8217;est pas à jour, ou votre thème, ou vos extensions.</p>\n<h2 id=\"comment-reparer\">Comment réparer ?</h2>\n<h3 id=\"1-acceder-a-ladministration-du-site\">1 &#8211; Accéder à l’administration du site</h3>\n<p>Essayez de récupérer l&#8217;accès à l’administration du site :</p>\n<ul>\n<li>En passant par FTP (Filezilla ou équivalent), ou dans le gestionnaire de fichiers de votre hébergeur, désactivez vos extensions en renommant le dossier <em>wp-content/plugins</em> (ajoutez un -X à la fin, par exemple)</li>\n<li>Renommez de la même façon votre thème qui se trouve dans <em>wp-content/themes</em> (il ne faut renommer que votre thème, pas le dossier complet)</li>\n</ul>\n<p>Si ces 2 opérations vous redonnent l&#8217;accès au site, une extension ou le thème était peut-être en cause. Renommez correctement les dossiers, puis réactivez les extensions une par une à partir de l&#8217;administration de votre site, en testant régulièrement. Si le problème réapparaît, vous avez trouvé l&#8217;extension coupable. Vérifiez si il existe une mise à jour, si ce n&#8217;est pas le cas, cherchez une autre extension qui propose les même fonctionnalités. Procédez de même pour le thème.</p>\n<p>Si vous n&#8217;avez toujours pas récupéré l&#8217;accès à l&#8217;administration du site, passez à l&#8217;étape suivante</p>\n<h3 id=\"2-verifier-les-utilisateurs-enregistres\">2 &#8211; Vérifier les utilisateurs enregistrés</h3>\n<p>Il s&#8217;agit à cette étape de vérifier si vos droits pour administrer le site ne sont pas corrompus ou usurpés.</p>\n<p>Avant toute intervention, sauvegardez votre base de données : connectez-vous à votre espace de gestion chez votre hébergeur, accédez à la base de données par PhpMyAdmin, et procédez à une sauvegarde (<a href=\"https://forums.cnetfrance.fr/tutoriels-reseaux-et-internet/199371-sauvegarder-et-restaurer-une-base-de-donnees-mysql-avec-phpmyadmin\">tuto</a> dont les images sont anciennes, mais déroulement à suivre)</p>\n<p>À présent, allez dans la table <em>wp_users</em> de la base de données (le préfixe <em>wp_</em> peut-être différent chez vous), vérifiez que votre pseudo est bien présent, et vérifiez qu’il n’y a pas d’utilisateur mystérieux. Allez ensuite dans la table <em>wp_usermeta</em> et vérifiez si votre pseudo est bien en “administrator” (colonne <em>meta_value</em>) au bout de la ligne qui contient les “capabilities” (colonne <em>meta_key</em>).</p>\n<p>Si dans la base de données, vous êtes toujours reconnu comme admin, modifiez simplement votre mot de passe comme indiqué dans le tuto de WPChannel cité en début d&#8217;article. Supprimez tout administrateur inconnu.</p>\n<p>Si vous n’êtes pas en mode admin, il faut absolument recréer un profil administrateur directement dans PhpMyAdmin (<a href=\"https://blogpascher.com/plugins-wordpress/comment-creer-un-administrateur-sur-wordpress-via-un-code-sql\">tuto</a>), vous pourrez alors vous connecter et supprimer les utilisateurs indésirables.</p>\n<p>Tant que vous êtes là, procédez au nettoyage de la base de données (<a href=\"https://www.youtube.com/watch?v=oEHYVtQbFKQ\">vidéo d&#8217;Alex, WP Marmite</a>).</p>\n<h3 id=\"3-modifier-vos-mots-de-passe\">3 &#8211; Modifier vos mots de passe</h3>\n<p>Changez maintenant vos mots de passe de connexion chez votre hébergeur : mot de passe pour vous connecter au panneau de gestion chez l&#8217;hébergeur, et mot de passe de connexion à la base de données (chez certains hébergeurs, c&#8217;est le même que pour le panneau de gestion).  Attention, notez bien le mot de passe pour l&#8217;accès à la base de données, car il faudra le modifier dans le fichier <em>wp-config.ph</em>p situé à la racine de l’installation, accessible par FTP (Filezilla ou équivalent) ou dans le gestionnaire de fichier de votre hébergeur.<br />\nAttention, c’est différent du mot de passe de connexion au site, qu’il faut modifier aussi si vous conservez votre ancien profil, comme vu à l&#8217;étape précédente.</p>\n<h3 id=\"4-augmenter-la-version-php\">4 &#8211; Augmenter la version PHP</h3>\n<p>Actuellement (décembre 2020), PHP est déjà accessible en version 8. Toutefois, les thèmes et extensions ne sont pas encore tous compatibles. Mais il est fortement conseillé de passer en PHP 7.3 ou PHP 7.4 si votre installation tourne avec une version inférieure.</p>\n<p>Accédez au panneau de gestion ou espace client chez votre hébergeur, et parcourez les options pour modifier la version PHP. Cela peut différer d&#8217;un hébergeur à l&#8217;autre (pour OVH, <a href=\"https://docs.ovh.com/fr/hosting/configurer-le-php-sur-son-hebergement-web-mutu-2014/\">suivez cette doc</a>).</p>\n<h3 id=\"5-sauvegarder-et-nettoyer-le-site\">5 &#8211; Sauvegarder et nettoyer le site</h3>\n<p>Par FTP (Filezilla) ou dans le gestionnaire de fichiers chez votre hébergeur, faites une sauvegarde du dossier <em>wp-content/uploads</em> (vous en aurez besoin pour la ré-installation). Sauvegardez aussi le fichier <em>wp-config.php</em> et le fichier <em>.htaccess<br />\n</em></p>\n<p>De même, faites une sauvegarde de votre ou vos thèmes personnalisés dans <em>wp-content/themes</em>. Supprimez tous les thèmes par défaut Twenty-XX, que vous pourrez ré-télécharger plus tard. Si votre thème provenait du répertoire officiel de wp.org et que vous n’avez pas modifié les fichiers des thèmes, supprimez-le. Attention, si c’est un thème payant, vérifiez si vous avez accès à une version saine. Ne téléchargez jamais &#8220;gratuitement&#8221; un thème payant, il contient probablement du code infecté.</p>\n<p>Notez la liste de toutes vos extensions présentes dans <em>wp-content/plugins</em>  : supprimez toutes celles qui viennent de wordpress.org. Pour les extensions payantes, vérifiez si vous avez accès à une version saine (neuve, à jour), et supprimez-les du dossier <em>wp-content/plugins</em>.</p>\n<p>Nettoyez les fichiers sauvegardés (<em>wp-config.php</em> et vos fichiers de thème) : pour cela, il faut les ouvrir avec NotePad++ ou SublimeText et supprimer la 1ère ligne composée d’une longue suite de chiffres et signes bizarres si c’est le cas, ou supprimez les espaces qui se trouveraient avant la balise d’ouverture <code>&lt;?php</code></p>\n<p>Vérifiez que le dossier <em>wp-content/uploads</em> ne contient aucun fichier <em>.php</em>, à l&#8217;exception de <em>index.php</em> quasi vide. Si vous trouvez un autre fichier en <em>.php</em>, supprimez le fichier suspect.</p>\n<h3 id=\"6-remettre-le-site-en-place\">6 &#8211; Remettre le site en place</h3>\n<p>À présent, il faut remettre en place le site avec des fichiers sains.</p>\n<ul>\n<li>Téléchargez sur votre ordi une version neuve de WP et dézippez-la. (Si vous n’aviez pas la toute dernière version de WP, voir ci-dessous)</li>\n<li>Par FTP (Filezilla), supprimez toute votre installation et renvoyez l’installation neuve.<br />\nA ce stade, ne touchez pas encore à votre site !</li>\n<li>Renvoyez par FTP votre fichier <em>wp-config.php</em> que vous aviez sauvegardé puis nettoyé, ainsi que vos fichiers du thème nettoyés à placer dans <em>wp-content/themes</em><br />\nAttention, ne renvoyez pas de fichier sans les avoir vérifiés et nettoyés du code malicieux</li>\n<li>Renvoyez votre dossier <em>wp-content/uploads</em> qui ne doit contenir que les dossiers par année/mois, et un fichier <em>index.php</em> (vérifiez qu’il ne soit pas infecté par le code malicieux). Ce dossier ne doit contenir aucun autre fichier <em>.php</em></li>\n<li>Téléchargez sur votre ordi toutes vos extensions à partir de <a href=\"https://fr.wordpress.org/download/\">wordpress.org</a> et dézippez-les (pour les payantes, adressez-vous à leur support). Envoyez les dossiers des extensions par FTP dans <em>wp-content/plugins </em></li>\n</ul>\n<h3 id=\"7-securiser-le-site\">7 &#8211; Sécuriser le site</h3>\n<p>Il reste quelques manipulations pour finir de sécuriser le site :</p>\n<ul>\n<li>Retournez sur votre admin’ de site et vérifiez qu’aucun utilisateur fantôme (sans adresse mail) ne soit inscrit, et surtout pas avec le statut administrateur (sinon, retour à l’étape de vérification des utilisateurs dans le base de données)</li>\n<li>Passez votre site en HTTPS avec l’extension <em>Really Simple SSL</em> comme expliqué <a href=\"https://brunotritsch.fr/passer-wordpress-https-quelques-clics-really-simple-ssl/\">ici</a></li>\n<li>Ré-enregistrez vos permaliens (Réglages &gt; Permaliens : il suffit de cliquer sur Sauvegarder) pour générer un nouveau fichier <em>.htaccess</em><br />\n(vous pourrez le comparer à celui sauvegardé en début de procédure, et rajouter les lignes supplémentaires qui vous paraissent nécessaires si besoin)</li>\n<li>Par FTP (Filezilla), téléchargez sur votre ordi le fichier .<em>htaccess</em> situé à la racine du site, là où se trouvent les 3 dossiers <em>wp-admin / wp-content / wp-includes</em>. Ouvrez-le avec avec NotePad++ ou équivalent, pour ajouter les lignes suivantes à la fin du fichier :<br />\n<div class=\"code-embed-wrapper\"> <pre class=\"language-less code-embed-pre line-numbers\"  data-start=\"1\" data-line-offset=\"0\"><code class=\"language-less code-embed-code\">&lt;IfModule mod_autoindex.c&gt;\nOptions -Indexes\n&lt;/IfModule&gt;</code></pre> <div class=\"code-embed-infos\"> </div> </div></li>\n<li>Renvoyez ensuite ce fichier .htaccess modifié par FTP à la racine du site, comme ci-dessus. Si Filezilla demande si il faut remplacer l’ancien fichier, acceptez.<br />\nL’ajout de ce code empêchera de lister le contenu des dossiers aux yeux des curieux. (inutile de faire savoir aux hackers toutes les extensions que vous utilisez, par exemple)</li>\n</ul>\n<h4>Encore quelques conseils</h4>\n<p>Si besoin, quelques conseils à suivre pour finir de nettoyer et sécuriser votre site :</p>\n<p>&#8211; <a href=\"https://www.iceranking.com/wordpress-seo/guide-complet-pour-nettoyer-et-securiser-wordpress-apres-un-hacking/\">https://www.iceranking.com/wordpress-seo/guide-complet-pour-nettoyer-et-securiser-wordpress-apres-un-hacking/</a> (tuto ancien mais toujours d’actualité)<br />\n&#8211; <a href=\"https://wpformation.com/wordpress-pirate-hack/\">https://wpformation.com/wordpress-pirate-hack/</a><br />\n&#8211; <a href=\"https://www.seomix.fr/securiser-wordpress-piratage/\">https://www.seomix.fr/securiser-wordpress-piratage/</a></p>\n<p>Vous trouverez dans ces articles un certain nombre de conseils déjà appliqués en suivant ce guide, mais n&#8217;hésitez pas à suivre les conseils supplémentaires.</p>\n<p>Et rappelez-vous qu&#8217;il faut maintenir à jour votre version de WordPress, votre ou vos thèmes, et toutes vos extensions. Des fonctionnalités récentes de WP 5.5.x et plus permettent d&#8217;automatiser ces mises à jour.<br />\nNe téléchargez jamais une extension provenant d&#8217;un site prétendant vous fournir gratuitement une extension habituellement payante.</p>\n<p>&nbsp;</p>\n<p>Nous voici à la fin de ce guide de nettoyage. Bon courage : <strong>il y a du travail, mais c’est le “prix à payer” </strong>pour être débarrassé du ou des pirates et retrouver un site fonctionnel.</p>\n<p>&nbsp;</p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=W2RcL0iquis:y4F4rgJekpc:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=W2RcL0iquis:y4F4rgJekpc:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=W2RcL0iquis:y4F4rgJekpc:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=W2RcL0iquis:y4F4rgJekpc:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=W2RcL0iquis:y4F4rgJekpc:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=W2RcL0iquis:y4F4rgJekpc:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/W2RcL0iquis\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:48:\"https://wpfr.net/site-hacke-ou-pirate-que-faire/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:1;a:6:{s:4:\"data\";s:57:\"\n		\n		\n		\n		\n		\n				\n		\n\n					\n										\n					\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:4:{s:0:\"\";a:6:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:82:\"WPFR : Compte rendu de réunion du Conseil d’administration du 21 septembre 2020\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/glC8A11w_uM/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Mon, 09 Nov 2020 11:40:56 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Association WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2358878\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:499:\"Le 21 septembre 2020, s’est déroulée la réunion du Conseil d’Administration de l’association WPFR. Étaient présents : &#8211; Les membres du CA : JB Audras Didier Demory Eric Martin Bruno Tritsch Valérie Galassi &#8211; Ainsi que : Jenny Dupuy Vincent Datin À l’issue de l’Assemblée Générale Ordinaire du 28 mai 2020 et suite à l’élection<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:7361:\"<p>Le 21 septembre 2020, s’est déroulée la réunion du Conseil d’Administration de l’association WPFR.</p>\n<p>Étaient présents :</p>\n<p>&#8211; Les membres du CA :</p>\n<ul>\n<li>JB Audras</li>\n<li>Didier Demory</li>\n<li>Eric Martin</li>\n<li>Bruno Tritsch</li>\n<li>Valérie Galassi</li>\n</ul>\n<p>&#8211; Ainsi que :</p>\n<ul>\n<li>Jenny Dupuy</li>\n<li>Vincent Datin</li>\n</ul>\n<p>À l’issue de l’Assemblée Générale Ordinaire du 28 mai 2020 et suite à l’élection du Conseil d’Administration, il a été décidé que chaque membre du CA choisirait un projet ayant pour but d’améliorer le site et d&#8217;apporter une valeur ajoutée au fait d’être membre de l’association.</p>\n<p>C’est pourquoi nous nous sommes réunis en septembre dernier pour que chacun choisisse un projet auquel il ou elle aimerait bien apporter sa participation.</p>\n<h2 id=\"les-projets-choisis-par-chaque-personne-ayant-participe-a-la-reunion\">Les projets choisis par chaque personne ayant participé à la réunion</h2>\n<h3 id=\"planet-wordpress-et-maintenance-du-site-wpfr\">Planet WordPress et maintenance du site WPFR</h3>\n<p>Didier et JB se sont proposés pour s’occuper de cette tâche.</p>\n<ul>\n<li>Le Planet est affiché sur la page d’accueil de ce site et permet aux visiteurs de retrouver les actualités WordPress, publiées par les membres de la communauté francophone, sur leurs propres blogs. Aujourd’hui le Planet rencontre des dysfonctionnements, de sorte que les actualités ne s’affichent pas correctement.</li>\n</ul>\n<ul>\n<li>Ils s’occuperont également de mettre en place une pré-prod du site pour effectuer les mises à jour des extensions et du cœur de WordPress ainsi que pour faire des tests.</li>\n</ul>\n<h3 id=\"centralisation-des-infos-dans-lorganisation-des-wordcamps\">Centralisation des infos dans l’organisation des WordCamps</h3>\n<p>Éric souhaite se pencher sur la mise en place d’une documentation et centraliser les informations des WordCamps afin d’aider et d’encourager leur organisation au sein de la communauté Francophone.<br />\nÀ chaque WordCamp, les organisateurs doivent aller à la pêche aux infos, aux prestataires, etc. Créer un guide en français regroupant la méthodologie ainsi que des ressources, mettre en place un forum dédié, afficher un calendrier des évènements, en cours de préparation ou programmés, permettra aux organisateurs de trouver facilement des informations et de l’aide.<br />\nIl ne s’agit pas de se substituer à la team Community de WordPress.org qui a un rôle très important et nous comptons bien travailler en concertation avec celle-ci.</p>\n<h3 id=\"formulaire-de-contact-du-site-wpfr\">Formulaire de contact du site WPFR</h3>\n<p>Jenny, Bruno, JB et moi-même, aimerions consacrer un peu de temps à cette tâche.</p>\n<p>Chaque jour nous recevons un nombre notable d’e-mails, provenant du formulaire de contact du site.</p>\n<p>Au-dessus de ce formulaire, il est spécifié ce qui suit :</p>\n<ul>\n<li style=\"list-style-type: none\">\n<ul>\n<li>Toute demande de support technique ne fera l’objet d’aucune réponse. Rendez-vous sur notre page<a href=\"https://wpfr.net/support/\"> Forum d’entraide</a> en cas de problème ;</li>\n<li>WordPress Francophone n’héberge pas de sites ;</li>\n<li>WordPress Francophone n’est pas responsable du contenu des sites utilisant le logiciel libre gratuit WordPress ;</li>\n<li>WordPress Francophone n’est pas lié au service WordPress.com.</li>\n<li>Pour toute demande de suppression de données personnelles, merci d’utiliser le<a href=\"https://wpfr.net/vos-donnees-personnelles/\"> formulaire dédié</a>.</li>\n</ul>\n</li>\n</ul>\n<p>Malgré cet avertissement, nous recevons, par exemple, des demandes :</p>\n<ol>\n<li>de personnes rencontrant des problèmes avec leurs sites personnels créés sous WordPress, croyant que WPFR est le support du CMS</li>\n<li>de personnes qui rencontrent des difficultés avec leurs comptes ou leurs sites sous wordpress.com</li>\n<li>de personnes qui souhaitent supprimer leur compte sur le site WPFR</li>\n</ol>\n<p>Etc.</p>\n<p>L’idée est donc de créer un formulaire de contact filtrant, pour que ces personnes trouvent directement les réponses à leurs demandes. Cela nous permettrait de passer moins de temps à traiter les e-mails et, par conséquent, nous consacrer à l’amélioration du site en général et à la gestion de l’association.</p>\n<h3 id=\"annuaire-des-adherents\">Annuaire des adhérents</h3>\n<p>Bruno va s’occuper de créer l’annuaire des adhérentes et adhérents à l’association. Cela apportera une valeur ajoutée au fait d’adhérer à notre association (en plus de bénéficier des coupons de nos partenaires). Cela permettrait à quiconque souhaitant trouver des prestataires pour la création de son site web, de trouver un interlocuteur. Nous recevons parfois, d’ailleurs, des e-mails de personnes qui en cherchent.</p>\n<p>C’est aussi une façon de le remercier de soutenir l’association.</p>\n<h3 id=\"certification-wordpress\">Certification WordPress</h3>\n<p>À l’heure actuelle il n’existe pas de certification WordPress (comme la certification Opquast, par exemple).</p>\n<p>Le sujet avait déjà été abordé il y a quelques années, mais n’a jamais abouti.</p>\n<p><strong>Si vous souhaitez participer à monter cette certification, n&#8217;hésitez pas à vous manifester.</strong></p>\n<h2 id=\"autres-sujets-abordes-lors-de-cette-reunion\">Autres sujets abordés lors de cette réunion</h2>\n<p>Nous aimerions donner une bonne lancée sur la communication de l’association, en communiquant plus sur Twitter, en écrivant des articles. Un prochain article sera rédigé en ce sens.</p>\n<p>Nous avons aussi évoqué l’idée de donner plus de droits aux modérateurs du forum pour qu’ils aient plus d’autonomie.</p>\n<p><strong>Si vous aussi, vous souhaitez rejoindre notre équipe, sur un sujet qui vous intéresse, faites-nous signe en commentaire de cet article, sur Twitter, ou encore sur Slack ! </strong></p>\n<p><em>Crédit photo, image à la Une :  <a href=\"https://unsplash.com/@marvelous?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Marvin Meyer</a> on <a href=\"https://unsplash.com/s/photos/work?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Unsplash</a></em></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=glC8A11w_uM:fbPLY0tXpUU:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=glC8A11w_uM:fbPLY0tXpUU:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=glC8A11w_uM:fbPLY0tXpUU:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=glC8A11w_uM:fbPLY0tXpUU:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=glC8A11w_uM:fbPLY0tXpUU:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=glC8A11w_uM:fbPLY0tXpUU:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/glC8A11w_uM\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:94:\"https://wpfr.net/wpfr-compte-rendu-de-reunion-du-conseil-dadministration-du-21-septembre-2020/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:2;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:66:\"Rendez-vous le 3 octobre 2020 pour le WordPress translation day FR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/Tmgk7D_ekIY/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:83:\"https://wpfr.net/rendez-vous-le-3-octobre-2020-pour-le-translation-day-fr/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 23 Sep 2020 09:43:18 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:11:\"Evènements\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2354153\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:481:\"Traduisons ! C&#8217;est ce que fera la communauté française WordPress le samedi 3 octobre 2020. Vous pouvez rejoindre les polyglottes et participer à ce mini-événement de la Journée de la traduction de WordPress en France. En 2020, un mini événement a déjà eu lieu en France le 17 avril, tandis qu&#8217;au Bangladesh on a encouragé<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3820:\"<p>Traduisons ! C&#8217;est ce que fera la communauté française WordPress le <strong>samedi 3 octobre 2020</strong>. Vous pouvez rejoindre les polyglottes et participer à ce mini-événement de la <strong>Journée de la traduction de WordPress en France.</strong></p>\n<p>En 2020, un mini événement a déjà eu lieu <a href=\"https://wpfr.net/et-si-vous-participiez-au-premier-wordpress-translation-day-fr/\">en France le 17 avril,</a> tandis qu&#8217;au Bangladesh on a encouragé la traduction de WordPress en bengali, <a href=\"https://wptranslationday.org/blog/encouraging-the-translation-of-wordpress-into-bengali/\">avec un atelier en ligne</a> qui a eu lieu le 16 mai.</p>\n<p>Cette fois-ci l&#8217;événement se déroulera <strong>samedi 3 octobre de 13h à 17h</strong> . Le sprint de traduction de quatre heures se déroulera en utilisant la plateforme de vidéoconférence Zoom et le Slack WordPress français.</p>\n<p>&nbsp;</p>\n<p style=\"text-align: center\"><img class=\"wp-image-2354256 aligncenter\" src=\"https://wpfr.net/files/2020/09/banniere.jpg\" alt=\"\" width=\"1301\" height=\"379\" /></p>\n<h2 id=\"comment-cela-va-t-il-se-derouler\">Comment cela va-t-il se dérouler ?</h2>\n<p>Durant cet après-midi de traduction 3 GTE (<em>General Translation Editors</em>) seront présents pour vous aider : Jenny Dupuy, FX Bénard et JB Audras.</p>\n<p>L&#8217;objectif est d&#8217;accueillir, orienter et accompagner un maximum de nouvelles personnes pour les initier à la traduction du cœur de WordPress, mais aussi des thèmes, des extensions et de la documentation relative au fonctionnement de ce CMS.</p>\n<p>Tout au long de l&#8217;après-midi les GTE accueilleront les personnes désireuses de participer et organiseront des mini-formations pour aider les personnes débutantes.</p>\n<p>Les personnes plus expérimentées pourront continuer ou commencer de traduire.</p>\n<h2 id=\"pourquoi-participer-et-que-traduire\">Pourquoi participer et que traduire ?</h2>\n<p>La traduction est une manière de <strong>contribuer au projet Open Source WordPress</strong>. Il est plus agréable et plus compréhensible d&#8217;utiliser WordPress, une extension ou un thème dans notre langue maternelle. C&#8217;est aussi plus inclusif, car il existe des personnes ne parlant pas anglais <img src=\"https://s.w.org/images/core/emoji/13.0.1/72x72/1f642.png\" alt=\"🙂\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></p>\n<p>Concernant ce que nous pouvons traduire, nous conseillons, en règle générale, de traduire des extensions que nous avons toutes et tous l&#8217;habitude d&#8217;utiliser.</p>\n<p>Nous vous attendons nombreuses et nombreux ! Alors rendez-vous sur le Slack de la communauté WordPress-fr : <a href=\"https://wpfr.net/slack/\">https://wpfr.net/slack/</a> puis dans le canal <strong>#wptranslationday</strong></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Tmgk7D_ekIY:5LkiyC5_xtg:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Tmgk7D_ekIY:5LkiyC5_xtg:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=Tmgk7D_ekIY:5LkiyC5_xtg:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Tmgk7D_ekIY:5LkiyC5_xtg:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Tmgk7D_ekIY:5LkiyC5_xtg:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=Tmgk7D_ekIY:5LkiyC5_xtg:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/Tmgk7D_ekIY\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:79:\"https://wpfr.net/rendez-vous-le-3-octobre-2020-pour-le-translation-day-fr/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:74:\"https://wpfr.net/rendez-vous-le-3-octobre-2020-pour-le-translation-day-fr/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:3;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:111:\"Bilan Assemblée Générale Ordinaire, élections du Conseil d’Administration et du Bureau de l’association\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/du9cx8dypdU/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:127:\"https://wpfr.net/bilan-assemblee-generale-ordinaire-elections-du-conseil-dadministration-et-du-bureau-de-lassociation/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Mon, 27 Jul 2020 16:46:12 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Association WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2349631\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:527:\"L&#8217;Assemblée Générale Ordinaire (AGO) de l&#8217;association WPFR a eu lieu le 28 mai 2020 en ligne. L&#8217;AGO était ouverte à tous les adhérents de l&#8217;association. La réunion a été enregistrée, je vous invite à la visionner sur notre chaîne YouTube (elle dure 1h33). Vous pouvez aussi consulter la présentation ici. Pendant l&#8217;Assemblée Générale nous avons<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3574:\"<p>L&#8217;Assemblée Générale Ordinaire (AGO) de l&#8217;association WPFR a eu lieu le 28 mai 2020 en ligne. L&#8217;AGO était ouverte à tous les adhérents de l&#8217;association.</p>\n<p>La réunion a été enregistrée, je vous invite <a href=\"https://www.youtube.com/watch?v=yYR27wn29nU&amp;feature=youtu.be\">à la visionner sur notre chaîne YouTube</a> (elle dure 1h33).</p>\n<p>Vous pouvez aussi consulter la présentation <a href=\"https://docs.google.com/presentation/d/1I9eefX_KHu3F4sFEtjWUC8FtH3Uji3nsFc2wuIeZT-8/edit?usp=sharing\">ici</a>.</p>\n<p>Pendant l&#8217;Assemblée Générale nous avons fait le point concernant nos actions par rapport à l&#8217;année écoulée. Nous avons aussi évoqué un point important : en effet en 2019-2020 l&#8217;association ne comportait que trois membres du Bureau et aucun Conseil d&#8217;Administration.</p>\n<p>Ainsi, nous avons dû gérer tout de front : les tâches administratives et les points relatifs à la vie de l&#8217;association.</p>\n<h2 id=\"election-du-conseil-dadministration\">Élection du Conseil d&#8217;Administration</h2>\n<p>A l&#8217;issue de la présentation, nous avons procédé à l&#8217;élection du Conseil d&#8217;Administration et c&#8217;est avec grand plaisir que je vous annonce que le Conseil compte huit membres :</p>\n<ol>\n<li>Amaury Balmer</li>\n<li>Bruno Tritsch</li>\n<li>Didier Demory</li>\n<li>Eric martin</li>\n<li>JB Audras</li>\n<li>Julien Lépine</li>\n<li>Julio Potier</li>\n<li>Valérie Galassi</li>\n</ol>\n<p>Les membres du CA choisissent un projet qu&#8217;ils veulent mener pour améliorer le site et apporter des améliorations à l&#8217;association et des avantages aux adhérents (voir la diapositive 21 de la présentation).</p>\n<p>Toutes personnes voulant participer au projet de l&#8217;association peuvent se faire connaître. Elles seront les bienvenues.</p>\n<h2 id=\"election-du-bureau\">Élection du Bureau</h2>\n<p>Le Conseil d&#8217;Administration a été sollicité pour participer à l&#8217;élection du nouveau Bureau pour 2020-2021.</p>\n<p>Faute de candidats, le Bureau a été maintenu tel quel pour la nouvelle année :</p>\n<ul>\n<li>Valérie Galassi, Présidente.</li>\n<li>Julio Potier, Trésorier</li>\n<li>Amaury Balmer, Secrétaire</li>\n</ul>\n<p>Crédit photo, image à la Une : <a href=\"https://unsplash.com/@dylandgillis?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Dylan Gillis</a> on <a href=\"https://unsplash.com/s/photos/meeting?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Unsplash</a></p>\n<p>&nbsp;</p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=du9cx8dypdU:pQT6tAKn2Y4:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=du9cx8dypdU:pQT6tAKn2Y4:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=du9cx8dypdU:pQT6tAKn2Y4:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=du9cx8dypdU:pQT6tAKn2Y4:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=du9cx8dypdU:pQT6tAKn2Y4:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=du9cx8dypdU:pQT6tAKn2Y4:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/du9cx8dypdU\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:123:\"https://wpfr.net/bilan-assemblee-generale-ordinaire-elections-du-conseil-dadministration-et-du-bureau-de-lassociation/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"6\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:118:\"https://wpfr.net/bilan-assemblee-generale-ordinaire-elections-du-conseil-dadministration-et-du-bureau-de-lassociation/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:4;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:47:\"Prochaine Assemblée Générale, le 28 mai 2020\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/CmfFqOWmGKo/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://wpfr.net/prochaine-assemblee-generale-le-28-mai-2020/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 14 May 2020 08:51:58 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Association WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2343090\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:475:\"Le mandat du Bureau actuel est terminé. Il est temps de procéder à son renouvellement. C&#8217;est pourquoi nous invitons les membres de l&#8217;association (dont la cotisation sera à jour en date du 20 mai 2020, date à laquelle la liste sera arrêtée) à participer à la prochaine Assemblée Générale qui se déroulera le 28 mai<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3003:\"<p>Le mandat du Bureau actuel est terminé. Il est temps de procéder à son renouvellement. C&#8217;est pourquoi nous invitons les membres de l&#8217;association (dont la cotisation sera à jour en date du 20 mai 2020, date à laquelle la liste sera arrêtée) à participer à la prochaine <strong>Assemblée Générale qui se déroulera </strong><br />\n<strong><a href=\"https://www.google.com/calendar/render?action=TEMPLATE&amp;text=ASSEMBL%C3%89E+G%C3%89N%C3%89RALE+WPFR&amp;dates=20200528T120000Z/20200528T140000Z&amp;ctz=Europe/Paris\">le 28 mai 2020, de 12h à 14h</a>.</strong></p>\n<p>Nous vous invitons à vous inscrire via un lien Zoom pour pouvoir y participer. Ci-joint <a href=\"https://docs.google.com/document/d/1ErKisPO7paGicpQ10JmumtdwBYF14HYRNHHCmeVBlOA/edit?usp=sharing\">la convocation</a> comportant le lien d&#8217;inscription.</p>\n<p>Les délibérations porteront sur l’ordre du jour suivant :</p>\n<ul>\n<li>Fixation du montant des cotisations ;</li>\n<li>Présentation du rapport d’activité ;</li>\n<li>Présentation du budget de l’association pour l’exercice en cours ;</li>\n<li>Renouvellement des membres du conseil d’administration ;</li>\n<li>Fixer les principales orientations du projet associatif</li>\n<li>Faire le point sur les difficultés rencontrées</li>\n<li>Questions diverses</li>\n</ul>\n<p>Si vous souhaitez rejoindre le Conseil d&#8217;administration et avoir la possibilité d&#8217;être élu pour participer au Bureau, vous pouvez le faire en postulant via les commentaires de cet article, mais aussi proposer votre candidature lors de l&#8217;assemblée.</p>\n<p>Pour rappel le Conseil d&#8217;administration n&#8217;est pas limité en nombre de personnes. Plus nous serons nombreux plus les projets de l’association avanceront efficacement.</p>\n<p>Nous procéderons à l&#8217;élection via la plateforme <a href=\"https://www.balotilo.org/login\">Balotilo</a>. Le vote anonyme se déroulera entre 12h30 et 13h, heure à laquelle nous annoncerons les résultats.</p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmfFqOWmGKo:-uQZzqsss74:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmfFqOWmGKo:-uQZzqsss74:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=CmfFqOWmGKo:-uQZzqsss74:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmfFqOWmGKo:-uQZzqsss74:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmfFqOWmGKo:-uQZzqsss74:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=CmfFqOWmGKo:-uQZzqsss74:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/CmfFqOWmGKo\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:66:\"https://wpfr.net/prochaine-assemblee-generale-le-28-mai-2020/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:61:\"https://wpfr.net/prochaine-assemblee-generale-le-28-mai-2020/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:5;a:6:{s:4:\"data\";s:57:\"\n		\n		\n		\n		\n		\n				\n		\n\n					\n										\n					\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:4:{s:0:\"\";a:6:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:64:\"Et si vous participiez au premier WordPress Translation Day FR ?\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/FrK6JhRA_8U/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Fri, 10 Apr 2020 16:03:37 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:11:\"Evènements\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2338016\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:505:\"Vous vous demandez peut-être ce qu’est le WordPress Translation Day FR ? D’habitude le WordPress Translation Day se déroule pendant 24 heures sur tous les fuseaux horaires. A cette occasion il est possible d&#8217;assister à des conférences en direct. Durant ces 24 heures les équipes de traduction de chaque langue apportent leurs pierres à l&#8217;édifice<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3519:\"<p>Vous vous demandez peut-être ce qu’est le <strong>WordPress Translation Day FR</strong> ?</p>\n<p>D’habitude le WordPress Translation Day se déroule pendant 24 heures sur tous les fuseaux horaires. A cette occasion il est possible d&#8217;assister à des conférences en direct. Durant ces 24 heures les équipes de traduction de chaque langue apportent leurs pierres à l&#8217;édifice en traduisant l’écosystème WordPress. Ils contribuent ainsi à la traduction du cœur, des extensions, des thèmes et de tous les sites de WordPress.org.</p>\n<p>Etant données les circonstances actuelles, cette année le premier <strong>WordPress Translation Day FR se déroulera en visioconférence, le 17 avril 2020</strong>, date à laquelle devait se dérouler le WordCamp Paris, qui a malheureusement été annulé.</p>\n<p>Les responsables de la traduction WordPress en français vous donnent <strong>rendez-vous dès 13 heures sur la <a href=\"https://zoom.us/meeting/register/vJQodeqhqjsopEDTK3ILgH2gYgFoDGQtdQ\">conférence Zoom</a>.</strong></p>\n<h2 id=\"pourquoi-participer-au-wordpress-translation-day\">Pourquoi participer au WordPress Translation day ?</h2>\n<p>Il y a plusieurs façons de <strong>contribuer au projet open source WordPress</strong>. On peut :</p>\n<ul>\n<li>améliorer le cœur du CMS</li>\n<li>organiser des évènements, comme des WordCamps ou des meetups.</li>\n<li>et on peut aussi traduire !</li>\n</ul>\n<h2 id=\"comment-participer-a-cet-evenement\">Comment participer à cet évènement ?</h2>\n<p>Si ce n’est déjà fait, inscrivez-vous sur le <a href=\"https://wpfr.net/slack/\">slack WordPress-fr</a>. L’équipe de traduction vous attend dans le canal <em>#wptranslationday !</em></p>\n<p>L&#8217;évènement se déroulera sur zoom, vous pouvez d&#8217;ores et déjà vous inscrire gratuitement : <a href=\"https://zoom.us/meeting/register/vJQodeqhqjsopEDTK3ILgH2gYgFoDGQtdQ\">https://zoom.us/meeting/register/vJQodeqhqjsopEDTK3ILgH2gYgFoDGQtdQ</a></p>\n<h2 id=\"le-programme-de-lapres-midi\">Le programme de l&#8217;après-midi</h2>\n<p>Rendez-vous dès 13 heures sur Zoom.</p>\n<p>Les responsables de traductions vous accueilleront et vous présenteront le travail en cours sur la traduction de la documentation WordPress en français.</p>\n<p>Tous et toutes ensembles nous contribuerons à la traduction d’extensions ou de thèmes.</p>\n<p>La conférence sera ouverte toute l’après-midi, vous pourrez donc rejoindre l’évènement à tout moment !</p>\n<p><strong>Rendez-vous le 17 avril !</strong></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=FrK6JhRA_8U:KQnwAUqtN7E:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=FrK6JhRA_8U:KQnwAUqtN7E:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=FrK6JhRA_8U:KQnwAUqtN7E:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=FrK6JhRA_8U:KQnwAUqtN7E:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=FrK6JhRA_8U:KQnwAUqtN7E:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=FrK6JhRA_8U:KQnwAUqtN7E:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/FrK6JhRA_8U\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:80:\"https://wpfr.net/et-si-vous-participiez-au-premier-wordpress-translation-day-fr/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:6;a:6:{s:4:\"data\";s:57:\"\n		\n		\n		\n		\n		\n				\n		\n\n					\n										\n					\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:4:{s:0:\"\";a:6:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:100:\"Enquête sur l’emploi &amp; les salaires dans l’écosystème WordPress en France (édition 2019)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/IqkX9CU7VW4/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 21 Jan 2020 12:31:18 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:21:\"WordPress Francophone\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2294460\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:546:\"L&#8217;objectif de cette enquête est de proposer un panorama annuel de l&#8217;écosystème WordPress en France. Cela permettra de mieux connaître les grandes tendances de l&#8217;emploi et des salaires des professionnels travaillant avec ce CMS. Ce genre d&#8217;étude est tout à fait courante dans d&#8217;autres secteurs d&#8217;activités, mais les résultats sont parfois payants. Ici, pas question,<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Valerie Galassi\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2802:\"<p>L&#8217;objectif de cette enquête est de proposer un panorama annuel de l&#8217;écosystème WordPress en France.<br />\nCela permettra de mieux connaître les grandes tendances de l&#8217;emploi et des salaires des professionnels travaillant avec ce CMS.</p>\n<p>Ce genre d&#8217;étude est tout à fait courante dans d&#8217;autres secteurs d&#8217;activités, mais les résultats sont parfois payants. Ici, pas question, <strong>les résultats seront publiés gratuitement chaque année</strong>.</p>\n<p>Pour cette première édition, l&#8217;enquête est très simple avec 15 questions facultatives. Cela devrait vous prendre quelques minutes tout au plus (c&#8217;est mieux si vous répondez à l&#8217;intégralité des questions).</p>\n<p>Et évidemment la <strong>participation est anonyme</strong>, vous pouvez donc tout dire sans crainte.</p>\n<p>Vous avez jusqu&#8217;au <strong>15 février 2019</strong> pour participer et <a href=\"https://www.altweb.fr/enquete-emploi-salaire-wordpress-2019/\">cela se passe ici </a><br />\n<strong>Les résultats</strong> seront annoncés dès la <strong>fin du mois de février</strong>.</p>\n<p>J&#8217;espère que vous serez nombreux à y participer !</p>\n<p>Merci <a href=\"https://twitter.com/clementbiron\">Clément Biron</a> d&#8217;avoir mis en place cette enquête annuelle ! Nous avons hâte de connaître les résultats <img src=\"https://s.w.org/images/core/emoji/13.0.1/72x72/1f642.png\" alt=\"🙂\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></p>\n<p><em>Crédit photo à la Une : <a href=\"https://unsplash.com/@joaosilas?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">João Silas</a> on <a href=\"https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Unsplash</a></em></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=IqkX9CU7VW4:84UaKzu5NXs:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=IqkX9CU7VW4:84UaKzu5NXs:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=IqkX9CU7VW4:84UaKzu5NXs:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=IqkX9CU7VW4:84UaKzu5NXs:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=IqkX9CU7VW4:84UaKzu5NXs:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=IqkX9CU7VW4:84UaKzu5NXs:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/IqkX9CU7VW4\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:94:\"https://wpfr.net/enquete-sur-lemploi-les-salaires-dans-ecosysteme-wordpress-en-france-et-2019/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:7;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:23:\"Les WordCamps en France\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/CmWTgxgCesc/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:50:\"https://wpfr.net/les-wordcamps-en-france/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 21 Aug 2019 12:55:20 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:8:\"WordCamp\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2274328\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:483:\"Nous avons récemment lancé un appel aux leaders de WordCamps afin de discuter des prochaines dates de leurs évènements et avons souhaité faire une sorte de récap des WordCamps possiblement organisables en France. À cette réunion étaient présents et présentes Valérie G. Amaury B., Julio P, F-X B., Marie C., Maylis L, les autres leads<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:5:\"Julio\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:7777:\"<p>Nous avons récemment lancé un appel aux leaders de WordCamps afin de discuter des prochaines dates de leurs évènements et avons souhaité faire une sorte de récap des WordCamps possiblement organisables en France.</p>\n<p>À cette réunion étaient présents et présentes Valérie G. Amaury B., Julio P, F-X B., Marie C., Maylis L, les autres leads n&#8217;ayant pas répondu à l&#8217;appel.</p>\n<p><span id=\"more-2274328\"></span></p>\n<h2 id=\"pourquoi\">Pourquoi ?</h2>\n<p>Dans le but d&#8217;éviter des chevauchements de date d&#8217;évènements, plus particulièrement en France ; car n&#8217;oublions pas que des WordCamps proches comme Angleterre, Belgique, Suisse et même Europe, US sont assez plébiscités par les membres de la communauté.</p>\n<p>Encore, il n&#8217;y a pas QUE les WordCamps, il y a aussi WPMX Day, ParisWeb, SudWeb, BlendWebMix, WPN Digital, WPTech, etc., ce qui fait que le nombre de visiteurs à un WordCamp dépend aussi de leur budget de déplacement annuel, plus il y a d&#8217;évènements, moins les gens sont présents partout !</p>\n<p>Aussi, la qualité de sponsoring dépend du montant alloué par chacun à chacun des WordCamps. Ainsi, plus il y a de WordCamps, moins il est possible aux sponsors de tous les financer, ou moins il est possible pour eux de souscrire à des formules élevées (Gold, Platine etc.).</p>\n<p>Puis, les sponsors réclament assez souvent un planning des WordCamps le plus tôt possible afin de pouvoir organiser plus facilement leur trésorerie, pouvoir faire un retour plus rapide et tôt aux organisateurs.</p>\n<h2 id=\"comment\">Comment ?</h2>\n<p>Lors de cette réunion, nous avons pensé à une solution réalisable qui serait de <strong>faire tourner l&#8217;organisation de WordCamps par secteur</strong>, pour laisser à la fois la chance à de nouvelles villes d&#8217;organiser un event, ainsi que pour éviter 2 WordCamps proches, que ce soit géographiquement ou dans le temps.</p>\n<p>Voici les secteurs avec les villes ayant déjà organisé un WordCamp associées :</p>\n<ul>\n<li><strong>Secteur A</strong> : Lille, Paris</li>\n<li><strong>Secteur B</strong> : Nice, Marseille, Aix</li>\n<li><strong>Secteur C</strong> : Lyon, (Genève, pas en France mais si proche qu&#8217;il faut faire attention à ne pas être en concurrence)</li>\n<li><strong>Secteur D</strong> : Bordeaux, Nantes</li>\n</ul>\n<p>Libre à d&#8217;autres organisateurs et organisatrices d&#8217;ajouter leur ville dans un secteur. Pour faire tourner les secteurs, l&#8217;idée est de faire <strong>3 WordCamps par an</strong> (<em>WPTech inclus en tant que WordCamp</em>) et de prendre <strong>1 ville par secteur</strong>. Puis l&#8217;année d&#8217;après, on recommence, on reprend 3 des 4 secteurs, une nouvelle ville, etc.</p>\n<p>Voici un brouillon possible de dates :</p>\n<ul>\n<li>Janvier : <strong>WCamp 1</strong></li>\n<li>Février</li>\n<li>Mars</li>\n<li>Avril: WordCamp London (fin avril)</li>\n<li>Mai : <strong>WCamp 2</strong> / SudWeb</li>\n<li>Juin : WordCamp Europe (début juin)</li>\n<li>Juillet : Rien</li>\n<li>Août : Rien</li>\n<li>Septembre</li>\n<li>Octobre : <strong>WCamp 3</strong> &#8211; Paris web</li>\n<li>Novembre : BlendWebMix</li>\n<li>Décembre</li>\n</ul>\n<h2 id=\"quand\">Quand ?</h2>\n<p>Maintenant. Pourquoi pas ! Dès à présent, il vous suffit de venir sur <a href=\"http://wordpressfr.slack.com\">le slack wordpressfr</a>, dans le canal <code>#wpfr</code>` afin de nous faire part de votre envie d&#8217;organiser un WordCamp, que ça soit une première édition ou pas.</p>\n<p>Nous vous proposons donc ainsi de centraliser les demandes françaises afin de poser un planning simple et rapide tous ensemble dans le but d&#8217;être certains et certaines que votre évènement n&#8217;entre pas en concurrence avec un autre évènement, WordCamp ou autre.</p>\n<h2 id=\"amelioration-de-lorganisation\">Amélioration de l&#8217;organisation</h2>\n<p>Quand on organise un WordCamp, beaucoup de choses se ressemblent, il y a un certain protocole à respecter, il est dicté par le WordCamp Central. WPFR est ravi d&#8217;aider ses adhérents grâce à toutes ses précédentes expériences, et aussi dans le sponsoring de votre WordCamp.</p>\n<p>Cependant, il y aussi d&#8217;autres points plus locaux à régler, comme le design et l&#8217;impression des badges, l&#8217;achat des goodies, le matériel vidéo/audio (j&#8217;y reviens juste après), le stockage de tout ça (idem), et la recherche de sponsors, la recherche des traiteurs, la recherche du lieu de l&#8217;event et de la soirée, etc.</p>\n<p>Pour tous ces points, WPFR s&#8217;engage à aider les organisateurs et organisatrices adhérant à notre suggestion, dans leurs démarches en leur fournissant des documents contenant la liste des personnes à contacter leur faisant ainsi gagner un temps précieux. Sur le slack nous (WPFR et les leads) serons aussi présents pour partager nos expériences pour vous aider dans vos démarches.</p>\n<p>Je reviens sur le point de la audio et vidéo, nous avons pensé nous procurer tout ce matériel au lieu de recevoir une valise de la part de Central sans vraiment savoir si ça fera l&#8217;affaire. Ceci serait acheté sur le compte de WPFR et prêté à n&#8217;importe quel personne adhérente de l&#8217;association qui en a besoin pour organiser un WordCamp.</p>\n<p>Pour le box, il s&#8217;agirait là d&#8217;avoir une personne dédiée par WordCamp capable de stocker temporairement des goodies, du matériels, des booths sponsor etc, cela évite de toujours avoir à chercher qui peut s&#8217;en charger.</p>\n<p>Du côté des sponsors, comme indiqué plus haut, c&#8217;est encore là notre but de pouvoir leur fournir notre calendrier prévisionnel. Si vous n&#8217;en faites pas partie, vous avez donc forcément moins de chances d&#8217;obtenir de l&#8217;aide de la part de certains. Et pour leur organisation de trésorerie, ce planning est plus que bienvenue !</p>\n<p>Durant l&#8217;évènement aussi nous pouvons aider à l&#8217;organisation, cela peut être dans le choix du nombre de tracks, du nombre de jours, du nombre de participants maximum. WPFR ferait le rôle d&#8217;un WP Central local dans votre organisation.</p>\n<h2 id=\"le-mot-de-le-fin\">Le mot de le fin</h2>\n<p>Attention, ceci n&#8217;est que <strong>suggestion</strong> et non pas une <strong>obligation</strong>, mais si tout le monde joue le jeu, alors il est fort probable que la qualité des évènements soient meilleures, que le sponsoring soit plus simple, que les visiteurs affluent plus simplement.</p>\n<p>Merci d&#8217;avance de faire passer le mot si vous entendez que quelqu&#8217;un souhaite organiser un WordCamp en France, merci de jouer le jeu avec nous, et bon (prochain) WordCamp !</p>\n<h2 id=\"\"></h2>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmWTgxgCesc:gbMYgRzO46U:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmWTgxgCesc:gbMYgRzO46U:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=CmWTgxgCesc:gbMYgRzO46U:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmWTgxgCesc:gbMYgRzO46U:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=CmWTgxgCesc:gbMYgRzO46U:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=CmWTgxgCesc:gbMYgRzO46U:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/CmWTgxgCesc\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:46:\"https://wpfr.net/les-wordcamps-en-france/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:2:\"12\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:41:\"https://wpfr.net/les-wordcamps-en-france/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:8;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:34:\"Liste des candidats au bureau 2019\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/OXrrV0szEo4/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:61:\"https://wpfr.net/liste-des-candidats-au-bureau-2019/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 24 Apr 2019 17:20:17 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Association WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2257794\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:482:\"Suite à l&#8217;appel à candidature qui s&#8217;est terminé le 21 avril 2019 à minuit, voici la liste des participants : Valérie Galassi Amaury Balmer Julio Potier Etant donné que : le nombre de candidats est de trois cette année, que le bureau doit être composé au minimum d&#8217;un Président, Trésorier et Secrétaire, et en accord<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:14:\"Benjamin Denis\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2099:\"<p>Suite à l&#8217;appel à candidature qui s&#8217;est terminé le 21 avril 2019 à minuit, voici la liste des participants :</p>\n<ul>\n<li>Valérie Galassi</li>\n<li>Amaury Balmer</li>\n<li>Julio Potier</li>\n</ul>\n<p>Etant donné que :</p>\n<ul>\n<li>le nombre de candidats est de trois cette année,</li>\n<li>que le bureau doit être composé au minimum d&#8217;un Président, Trésorier et Secrétaire,</li>\n<li>et en accord avec le Président actuel (Julio Potier),</li>\n</ul>\n<p>Il a été décidé de passer outre les élections (vu que tout le monde est élu d&#8217;office).</p>\n<p>L&#8217;actuel bureau va donc être dissous pour laisser la place aux nouveaux membres.</p>\n<p>La répartition des rôles se fera lors d&#8217;une réunion interne selon les envies de chacun.</p>\n<p><strong>Merci à Willy Bahuaud (Vice Président), Aurélien Denis (Trésorier) et Benjamin Denis (Secrétaire) pour leur travail effectué ces dernières années et bienvenue à la nouvelle équipe dont l&#8217;actuel Président, Julio Potier, Amaury Balmer (fondateur de l&#8217;asso) et à Valérie Galassi !</strong></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=OXrrV0szEo4:PP7KZOQ07VE:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=OXrrV0szEo4:PP7KZOQ07VE:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=OXrrV0szEo4:PP7KZOQ07VE:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=OXrrV0szEo4:PP7KZOQ07VE:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=OXrrV0szEo4:PP7KZOQ07VE:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=OXrrV0szEo4:PP7KZOQ07VE:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/OXrrV0szEo4\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:57:\"https://wpfr.net/liste-des-candidats-au-bureau-2019/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"4\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:52:\"https://wpfr.net/liste-des-candidats-au-bureau-2019/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:9;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:6:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:46:\"Compte rendu de l’assemblée générale 2018\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"http://feedproxy.google.com/~r/WordpressFrancophone/~3/Stm97xB9WpM/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"https://wpfr.net/compte-rendu-de-lassemblee-generale-2018/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 09 May 2019 12:13:45 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Association WPFR\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:27:\"https://wpfr.net/?p=2256946\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:505:\"L&#8217;Assemblée Générale Ordinaire a été réalisée le mercredi 03 avril 2019. Les membres présents étaient Aurélien Denis, Benjamin Denis, Willy Bahuaud et Julio Potier. Ordre du jour : Bilan moral Bilan financier Projets en cours et à venir Renouvellement du bureau Réunions Rapport moral Que retenir de cette année 2018 ? Des adhérents en augmentation<div class=\"btn btn-default read-more text-uppercase\">Lire la suite <span class=\"meta-nav\"><i class=\"fa fa-caret-right\"></i></span></div>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:5:\"Julio\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:6605:\"<p>L&#8217;Assemblée Générale Ordinaire a été réalisée le mercredi 03 avril 2019. Les membres présents étaient Aurélien Denis, Benjamin Denis, Willy Bahuaud et Julio Potier.<br />\n<span id=\"more-2256946\"></span></p>\n<p>Ordre du jour :</p>\n<ul>\n<li>Bilan moral</li>\n<li>Bilan financier</li>\n<li>Projets en cours et à venir</li>\n<li>Renouvellement du bureau</li>\n<li>Réunions</li>\n</ul>\n<h2 id=\"rapport-moral\">Rapport moral</h2>\n<h3 id=\"que-retenir-de-cette-annee-2018\">Que retenir de cette année 2018 ?</h3>\n<p>Des adhérents en augmentation : l’association a passé le seuil des 200 membres avec 214 au jour de l’Assemblée Générale soit une augmentation de 72 % en un an.</p>\n<h3 id=\"un-site-web-plebiscite\">Un site web plébiscité</h3>\n<p>Le site web qui a maintenant pris place il y a quelques temps a bel et bien trouvé sa place, apprécié de toute la communauté, il grandit, il vit, il évolue.</p>\n<p>100 000 visiteurs par mois en moyenne (dont 5000 / jour ouvré) et 300 000 pages vues mensuelles, soit près de 3,4 millions de pages vues en un an ! On peut être satisfait du travail accompli. La transition entre l’ancien site et le nouveau est donc une réussite. Les utilisateurs nous ont fait confiance et nous sont restés fidèles. Merci à tous !</p>\n<p>Un phénomène intéressant est à noter dans les visites du site, une activité en baisse tous les weekends de presque 50 %. Cela se comprend bien, on ne travaille pas le weekend, on ne pose pas (moins) de question sur le forum !</p>\n<p>Le forum de support continue de confirmer son statut de point central de la communauté puisqu’il est le lieu favori des visiteurs du site. Il représente 76 % des pages vues annuelles.</p>\n<p>Vous appréciez toujours autant les fonctionnalités comme l’agenda dynamique des manifestations, la rubrique emploi, un portail toujours plus communautaire, etc.</p>\n<p>Notons l’évolution régulière des abonnés sur les réseaux sociaux :</p>\n<ul>\n<li><a href=\"https://twitter.com/wordpress_fr\">Twitter</a> : 7 497 abonnés</li>\n<li><a href=\"https://www.facebook.com/WordPressFrancophone\">Facebook</a> : 2 604 amis</li>\n<li><a href=\"https://www.youtube.com/channel/UClMKlcMoMzvVL_9Rn6i9Gvw/\">YouTube</a> : 58 abonnés</li>\n</ul>\n<p>N’hésitez pas à nous suivre si ce n’est pas encore le cas.</p>\n<h2 id=\"bilan-financier\">Bilan financier</h2>\n<p>Au 31/12/2018, nous avions sur le compte courant 6070,29 € et 15 051,56 € sur le livret A.</p>\n<h3 id=\"les-recettes-proviennent-des\">Les recettes proviennent des :</h3>\n<p>&#8211; Adhésions particuliers : 3 750 €<br />\n&#8211; Adhésions professionnels : 1 500 €<br />\n&#8211; Dons / sponsoring de meetups : 433 € (y a du o2switch, SEOPress, dons divers)</p>\n<h3 id=\"les-depenses-sont\">Les dépenses sont :</h3>\n<p>&#8211; Sponsoring d’évènements (WordCamps, meetups, hors communauté) : 1 836 €<br />\n&#8211; Frais divers (banque, postaux, stickers, roll-up) : 446,44 €<br />\n&#8211; Assurance RC : 246,82 €<br />\n&#8211; Ancien Hébergement PlanetHoster : 99,19 €</p>\n<p>Il y a donc un excédent de 3 054,55 € en 2018.</p>\n<h2 id=\"des-projets-annonces-et-des-projets-realises\">Des projets annoncés et des projets réalisés</h2>\n<ol>\n<li><strong>Certification WordPress</strong> : le projet n&#8217;avance plus ;</li>\n<li><strong>Projet de fédération</strong> : flop ! L&#8217;idée était de faire en sorte que chaque meetup, chaque groupe de communauté ait un représentant qui représente l&#8217;association là où il est. L&#8217;idée est bonne et tient toujours, juste que nous n&#8217;avons pas réussi à mettre en oeuvre tout ça, pas encore, 2019 peut-être ?</li>\n<li><strong>Partenaire des WordCamps</strong> : plusieurs événements soutenus en 2017 avec le WCEU en point d’orgue. Divers soutiens ont été mis en place en 2017 que ce soit financier ou logistique, notamment en étant association support pour plusieurs événements afin de couvrir l’assurance nécessaires aux organisateurs. Il est d’ailleurs rappelé aux communautés locales de ne pas hésiter à faire appel à nous pour cela. Nous avons la structure et l’assurance adéquate ;</li>\n<li><strong>Les communautés locales</strong> : malgré un gros retard, le sondage destiné aux communautés locales est finalisé, il devrait être envoyé aux responsables de communautés dans le courant du mois de février ;</li>\n<li><strong>Communication</strong> : toutes les attentions ont été portées sur le site web, les réseaux sociaux (FB et Twitter) et les événements. En 2019, on va monter en puissance sur la communication web et développer la communication dans le monde réel.</li>\n</ol>\n<h2 id=\"renouvellement-du-bureau\">Renouvellement du bureau</h2>\n<p>Le jeudi 02 mai 2019 nous avons officialisé les rôles des 3 personnes (seulement) ayant postulé au bureau WPFR.</p>\n<ul>\n<li>Présidente : Valérie Galassi</li>\n<li>Secrétaire : Amaury Balmer</li>\n<li>Trésorier : Julio Potier</li>\n</ul>\n<p>Chacun a émis un souhait d&#8217;un rôle et un veto d&#8217;un autre, chacun a réussi au premier essai à avoir le poste souhaité !</p>\n<h2 id=\"reunions\">Réunions</h2>\n<p>Nous avons décidé de nous réunir plus souvent pour faire le point et discuter de nouveaux points si besoin. Toutes les 2 semaines le jeudi matin si un ordre du jour est précisé. Nous ferons donc (merci Amaury) aussi plus de comptes rendus sur l&#8217;association.</p>\n<p>Bonne route à toutes et à tous pour 2019 !</p>\n<p><em>Image à la une : <a href=\"https://unsplash.com/@carlheyerdahl\">@carlheyerdahl</a></em></p>\n<div class=\"feedflare\">\n<a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Stm97xB9WpM:3418vZ02tO0:yIl2AUoC8zA\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=yIl2AUoC8zA\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Stm97xB9WpM:3418vZ02tO0:V_sGLiPBpWU\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=Stm97xB9WpM:3418vZ02tO0:V_sGLiPBpWU\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Stm97xB9WpM:3418vZ02tO0:qj6IDK7rITs\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?d=qj6IDK7rITs\" border=\"0\"></img></a> <a href=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?a=Stm97xB9WpM:3418vZ02tO0:gIN9vFwOqvQ\"><img src=\"http://feeds.feedburner.com/~ff/WordpressFrancophone?i=Stm97xB9WpM:3418vZ02tO0:gIN9vFwOqvQ\" border=\"0\"></img></a>\n</div><img src=\"http://feeds.feedburner.com/~r/WordpressFrancophone/~4/Stm97xB9WpM\" height=\"1\" width=\"1\" alt=\"\"/>\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:63:\"https://wpfr.net/compte-rendu-de-lassemblee-generale-2018/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:1:{s:8:\"origLink\";a:1:{i:0;a:5:{s:4:\"data\";s:58:\"https://wpfr.net/compte-rendu-de-lassemblee-generale-2018/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}s:44:\"http://purl.org/rss/1.0/modules/syndication/\";a:2:{s:12:\"updatePeriod\";a:1:{i:0;a:5:{s:4:\"data\";s:9:\"\n	hourly	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:15:\"updateFrequency\";a:1:{i:0;a:5:{s:4:\"data\";s:4:\"\n	1	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:27:\"http://www.w3.org/2005/Atom\";a:1:{s:4:\"link\";a:2:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:3:\"rel\";s:4:\"self\";s:4:\"type\";s:19:\"application/rss+xml\";s:4:\"href\";s:48:\"http://feeds.feedburner.com/WordpressFrancophone\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:3:\"rel\";s:3:\"hub\";s:4:\"href\";s:32:\"http://pubsubhubbub.appspot.com/\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:42:\"http://rssnamespace.org/feedburner/ext/1.0\";a:4:{s:4:\"info\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:3:\"uri\";s:20:\"wordpressfrancophone\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:14:\"emailServiceId\";a:1:{i:0;a:5:{s:4:\"data\";s:20:\"WordpressFrancophone\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:18:\"feedburnerHostname\";a:1:{i:0;a:5:{s:4:\"data\";s:29:\"https://feedburner.google.com\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:9:\"feedFlare\";a:9:{i:0;a:5:{s:4:\"data\";s:24:\"Subscribe with NewsGator\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:112:\"http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:42:\"http://www.newsgator.com/images/ngsub1.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:24:\"Subscribe with Bloglines\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:77:\"http://www.bloglines.com/sub/http://feeds.feedburner.com/WordpressFrancophone\";s:3:\"src\";s:48:\"http://www.bloglines.com/images/sub_modern11.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:2;a:5:{s:4:\"data\";s:23:\"Subscribe with Netvibes\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:98:\"http://www.netvibes.com/subscribe.php?url=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:39:\"//www.netvibes.com/img/add2netvibes.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:3;a:5:{s:4:\"data\";s:21:\"Subscribe with Google\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:93:\"http://fusion.google.com/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:51:\"http://buttons.googlesyndication.com/fusion/add.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:4;a:5:{s:4:\"data\";s:25:\"Subscribe with Pageflakes\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:101:\"http://www.pageflakes.com/subscribe.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:87:\"http://www.pageflakes.com/ImageFile.ashx?instanceId=Static_4&fileName=ATP_blu_91x17.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:5;a:5:{s:4:\"data\";s:21:\"Subscribe with Plusmo\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:86:\"http://www.plusmo.com/add?url=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:43:\"http://plusmo.com/res/graphics/fbplusmo.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:6;a:5:{s:4:\"data\";s:23:\"Subscribe with Live.com\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:81:\"http://www.live.com/?add=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:141:\"http://tkfiles.storage.msn.com/x1piYkpqHC_35nIp1gLE68-wvzLZO8iXl_JMledmJQXP-XTBOLfmQv4zhj4MhcWEJh_GtoBIiAl1Mjh-ndp9k47If7hTaFno0mxW9_i3p_5qQw\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:7;a:5:{s:4:\"data\";s:25:\"Subscribe with Mon Yahoo!\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:99:\"https://add.my.yahoo.com/content?lg=fr&url=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:60:\"http://us.i1.yimg.com/us.yimg.com/i/us/my/bn/intatm_fr_1.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:8;a:5:{s:4:\"data\";s:25:\"Subscribe with Excite MIX\";s:7:\"attribs\";a:1:{s:0:\"\";a:2:{s:4:\"href\";s:89:\"http://mix.excite.eu/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2FWordpressFrancophone\";s:3:\"src\";s:42:\"http://image.excite.co.uk/mix/addtomix.gif\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:52:\"http://backend.userland.com/creativeCommonsRssModule\";a:1:{s:7:\"license\";a:1:{i:0;a:5:{s:4:\"data\";s:49:\"http://creativecommons.org/licenses/by-nc-sa/3.0/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}}}}}}s:4:\"type\";i:128;s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:11:{s:12:\"content-type\";s:23:\"text/xml; charset=UTF-8\";s:4:\"etag\";s:27:\"BeeB6pFy2j9J98nJujnXRRqjocg\";s:13:\"last-modified\";s:29:\"Mon, 08 Mar 2021 09:04:31 GMT\";s:16:\"content-encoding\";s:4:\"gzip\";s:4:\"date\";s:29:\"Mon, 08 Mar 2021 09:15:33 GMT\";s:7:\"expires\";s:29:\"Mon, 08 Mar 2021 09:15:33 GMT\";s:13:\"cache-control\";s:18:\"private, max-age=0\";s:22:\"x-content-type-options\";s:7:\"nosniff\";s:16:\"x-xss-protection\";s:13:\"1; mode=block\";s:6:\"server\";s:3:\"GSE\";s:7:\"alt-svc\";s:167:\"h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"\";}}s:5:\"build\";s:14:\"20210305191220\";}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(556, '_transient_timeout_feed_mod_3ca2a73478cc83bbe37e39039b345a78', '1615238076', 'no'),
(557, '_transient_feed_mod_3ca2a73478cc83bbe37e39039b345a78', '1615194876', 'no'),
(558, '_site_transient_timeout_community-events-a88992eafe48f70749e524e083590722', '1615238077', 'no'),
(559, '_site_transient_community-events-a88992eafe48f70749e524e083590722', 'a:4:{s:9:\"sandboxed\";b:0;s:5:\"error\";N;s:8:\"location\";a:1:{s:2:\"ip\";s:12:\"192.168.99.0\";}s:6:\"events\";a:1:{i:0;a:10:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:41:\"(español) Seguridad básica en WordPress\";s:3:\"url\";s:68:\"https://www.meetup.com/learn-wordpress-discussions/events/276606607/\";s:6:\"meetup\";s:27:\"Learn WordPress Discussions\";s:10:\"meetup_url\";s:51:\"https://www.meetup.com/learn-wordpress-discussions/\";s:4:\"date\";s:19:\"2021-03-10 03:00:00\";s:8:\"end_date\";s:19:\"2021-03-10 04:00:00\";s:20:\"start_unix_timestamp\";i:1615374000;s:18:\"end_unix_timestamp\";i:1615377600;s:8:\"location\";a:4:{s:8:\"location\";s:6:\"Online\";s:7:\"country\";s:2:\"US\";s:8:\"latitude\";d:37.779998779297;s:9:\"longitude\";d:-122.41999816895;}}}}', 'no'),
(560, '_transient_timeout_dash_v2_bd94b8f41e74bae2f4dc72e9bd8379af', '1615238079', 'no'),
(561, '_transient_dash_v2_bd94b8f41e74bae2f4dc72e9bd8379af', '<div class=\"rss-widget\"><ul><li><a class=\'rsswidget\' href=\'http://feedproxy.google.com/~r/WordpressFrancophone/~3/W2RcL0iquis/\'>Site hacké ou piraté : que faire ?</a></li></ul></div><div class=\"rss-widget\"><p><strong>Erreur RSS :</strong> A feed could not be found at https://feeds.feedburner.com/wpfr. A feed with an invalid mime type may fall victim to this error, or SimplePie was unable to auto-discover it.. Use force_feed() if you are certain this URL is a real feed.</p></div>', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(61, 754, '_wp_attached_file', '2011/07/100_5478.jpg'),
(62, 754, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:20:\"2011/07/100_5478.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"100_5478-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"100_5478-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"100_5478-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"100_5478-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:22:\"100_5478-1600x1200.jpg\";s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"100_5478-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(63, 754, '_wp_attachment_image_alt', 'Bell on Wharf'),
(64, 754, '_wp_attachment_image_alt', 'Bell on Wharf'),
(65, 754, '_attachment_original_parent_id', '555'),
(91, 760, '_wp_attached_file', '2011/07/dsc09114.jpg'),
(92, 760, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:20:\"2011/07/dsc09114.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"dsc09114-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"dsc09114-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"dsc09114-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"dsc09114-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:22:\"dsc09114-1600x1200.jpg\";s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"dsc09114-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(93, 760, '_wp_attachment_image_alt', 'Sydney Harbor Bridge'),
(94, 760, '_wp_attachment_image_alt', 'Sydney Harbor Bridge'),
(95, 760, '_attachment_original_parent_id', '555'),
(208, 1046, '_menu_item_type', 'custom'),
(209, 1046, '_menu_item_menu_item_parent', '0'),
(210, 1046, '_menu_item_object_id', '1046'),
(211, 1046, '_menu_item_object', 'custom'),
(212, 1046, '_menu_item_target', ''),
(213, 1046, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(214, 1046, '_menu_item_xfn', ''),
(215, 1046, '_menu_item_url', '#'),
(216, 1047, '_menu_item_type', 'custom'),
(217, 1047, '_menu_item_menu_item_parent', '0'),
(218, 1047, '_menu_item_object_id', '1047'),
(219, 1047, '_menu_item_object', 'custom'),
(220, 1047, '_menu_item_target', ''),
(221, 1047, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(222, 1047, '_menu_item_xfn', ''),
(223, 1047, '_menu_item_url', '#'),
(232, 1049, '_menu_item_type', 'taxonomy'),
(233, 1049, '_menu_item_menu_item_parent', '1047'),
(234, 1049, '_menu_item_object_id', '135'),
(235, 1049, '_menu_item_object', 'category'),
(236, 1049, '_menu_item_target', ''),
(237, 1049, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(238, 1049, '_menu_item_xfn', ''),
(239, 1049, '_menu_item_url', ''),
(248, 1051, '_menu_item_type', 'custom'),
(249, 1051, '_menu_item_menu_item_parent', '0'),
(250, 1051, '_menu_item_object_id', '1051'),
(251, 1051, '_menu_item_object', 'custom'),
(252, 1051, '_menu_item_target', ''),
(253, 1051, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(254, 1051, '_menu_item_xfn', ''),
(255, 1051, '_menu_item_url', '#'),
(256, 1052, '_menu_item_type', 'custom'),
(257, 1052, '_menu_item_menu_item_parent', '1051'),
(258, 1052, '_menu_item_object_id', '1052'),
(259, 1052, '_menu_item_object', 'custom'),
(260, 1052, '_menu_item_target', ''),
(261, 1052, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(262, 1052, '_menu_item_xfn', ''),
(263, 1052, '_menu_item_url', '#'),
(264, 1053, '_menu_item_type', 'custom'),
(265, 1053, '_menu_item_menu_item_parent', '1052'),
(266, 1053, '_menu_item_object_id', '1053'),
(267, 1053, '_menu_item_object', 'custom'),
(268, 1053, '_menu_item_target', ''),
(269, 1053, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(270, 1053, '_menu_item_xfn', ''),
(271, 1053, '_menu_item_url', '#'),
(272, 1054, '_menu_item_type', 'custom'),
(273, 1054, '_menu_item_menu_item_parent', '1053'),
(274, 1054, '_menu_item_object_id', '1054'),
(275, 1054, '_menu_item_object', 'custom'),
(276, 1054, '_menu_item_target', ''),
(277, 1054, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(278, 1054, '_menu_item_xfn', ''),
(279, 1054, '_menu_item_url', '#'),
(280, 1055, '_menu_item_type', 'custom'),
(281, 1055, '_menu_item_menu_item_parent', '1054'),
(282, 1055, '_menu_item_object_id', '1055'),
(283, 1055, '_menu_item_object', 'custom'),
(284, 1055, '_menu_item_target', ''),
(285, 1055, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(286, 1055, '_menu_item_xfn', ''),
(287, 1055, '_menu_item_url', '#'),
(288, 1056, '_menu_item_type', 'custom'),
(289, 1056, '_menu_item_menu_item_parent', '1055'),
(290, 1056, '_menu_item_object_id', '1056'),
(291, 1056, '_menu_item_object', 'custom'),
(292, 1056, '_menu_item_target', ''),
(293, 1056, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(294, 1056, '_menu_item_xfn', ''),
(295, 1056, '_menu_item_url', '#'),
(296, 1057, '_menu_item_type', 'custom'),
(297, 1057, '_menu_item_menu_item_parent', '1056'),
(298, 1057, '_menu_item_object_id', '1057'),
(299, 1057, '_menu_item_object', 'custom'),
(300, 1057, '_menu_item_target', ''),
(301, 1057, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(302, 1057, '_menu_item_xfn', ''),
(303, 1057, '_menu_item_url', '#'),
(304, 1058, '_menu_item_type', 'custom'),
(305, 1058, '_menu_item_menu_item_parent', '1057'),
(306, 1058, '_menu_item_object_id', '1058'),
(307, 1058, '_menu_item_object', 'custom'),
(308, 1058, '_menu_item_target', ''),
(309, 1058, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(310, 1058, '_menu_item_xfn', ''),
(311, 1058, '_menu_item_url', '#'),
(312, 1059, '_menu_item_type', 'custom'),
(313, 1059, '_menu_item_menu_item_parent', '1058'),
(314, 1059, '_menu_item_object_id', '1059'),
(315, 1059, '_menu_item_object', 'custom'),
(316, 1059, '_menu_item_target', ''),
(317, 1059, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(318, 1059, '_menu_item_xfn', ''),
(319, 1059, '_menu_item_url', '#'),
(320, 1060, '_menu_item_type', 'custom'),
(321, 1060, '_menu_item_menu_item_parent', '1059'),
(322, 1060, '_menu_item_object_id', '1060'),
(323, 1060, '_menu_item_object', 'custom'),
(324, 1060, '_menu_item_target', ''),
(325, 1060, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(326, 1060, '_menu_item_xfn', ''),
(327, 1060, '_menu_item_url', '#'),
(328, 1061, '_menu_item_type', 'custom'),
(329, 1061, '_menu_item_menu_item_parent', '1060'),
(330, 1061, '_menu_item_object_id', '1061'),
(331, 1061, '_menu_item_object', 'custom'),
(332, 1061, '_menu_item_target', ''),
(333, 1061, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(334, 1061, '_menu_item_xfn', ''),
(335, 1061, '_menu_item_url', '#'),
(336, 1062, '_menu_item_type', 'custom'),
(337, 1062, '_menu_item_menu_item_parent', '0'),
(338, 1062, '_menu_item_object_id', '1062'),
(339, 1062, '_menu_item_object', 'custom'),
(340, 1062, '_menu_item_target', ''),
(341, 1062, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(342, 1062, '_menu_item_xfn', ''),
(343, 1062, '_menu_item_url', '#'),
(344, 1063, '_menu_item_type', 'custom'),
(345, 1063, '_menu_item_menu_item_parent', '0'),
(346, 1063, '_menu_item_object_id', '1063'),
(347, 1063, '_menu_item_object', 'custom'),
(348, 1063, '_menu_item_target', ''),
(349, 1063, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(350, 1063, '_menu_item_xfn', ''),
(351, 1063, '_menu_item_url', '#'),
(352, 1064, '_menu_item_type', 'custom'),
(353, 1064, '_menu_item_menu_item_parent', '1062'),
(354, 1064, '_menu_item_object_id', '1064'),
(355, 1064, '_menu_item_object', 'custom'),
(356, 1064, '_menu_item_target', ''),
(357, 1064, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(358, 1064, '_menu_item_xfn', ''),
(359, 1064, '_menu_item_url', '#'),
(360, 1065, '_menu_item_type', 'custom'),
(361, 1065, '_menu_item_menu_item_parent', '1062'),
(362, 1065, '_menu_item_object_id', '1065'),
(363, 1065, '_menu_item_object', 'custom'),
(364, 1065, '_menu_item_target', ''),
(365, 1065, '_menu_item_classes', 'a:1:{i:0;s:21:\"custom-menu-css-class\";}'),
(366, 1065, '_menu_item_xfn', ''),
(367, 1065, '_menu_item_url', '#'),
(368, 1066, '_menu_item_type', 'custom'),
(369, 1066, '_menu_item_menu_item_parent', '1062'),
(370, 1066, '_menu_item_object_id', '1066'),
(371, 1066, '_menu_item_object', 'custom'),
(372, 1066, '_menu_item_target', '_blank'),
(373, 1066, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(374, 1066, '_menu_item_xfn', ''),
(375, 1066, '_menu_item_url', 'http://apple.com'),
(378, 1629, '_menu_item_type', 'custom'),
(379, 1629, '_menu_item_menu_item_parent', '0'),
(380, 1629, '_menu_item_object_id', '1629'),
(381, 1629, '_menu_item_object', 'custom'),
(382, 1629, '_menu_item_target', ''),
(383, 1629, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(384, 1629, '_menu_item_xfn', ''),
(385, 1629, '_menu_item_url', 'http://wpthemetestdata.wordpress.com/'),
(555, 733, '_wp_page_template', 'default'),
(592, 1016, '_wp_old_slug', 'media-featured-image-vertical'),
(593, 1016, '_wp_old_slug', 'featured-image-vertical'),
(594, 1016, '_publicize_pending', '1'),
(596, 1016, 'standard_seo_post_level_layout', ''),
(597, 1016, 'standard_link_url_field', ''),
(598, 1016, 'standard_seo_post_meta_description', ''),
(599, 1016, 'original_post_id', '1016'),
(600, 1016, '_wp_old_slug', '1016'),
(817, 1703, '_menu_item_type', 'post_type'),
(818, 1703, '_menu_item_menu_item_parent', '0'),
(819, 1703, '_menu_item_object_id', '733'),
(820, 1703, '_menu_item_object', 'page'),
(821, 1703, '_menu_item_target', ''),
(822, 1703, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(823, 1703, '_menu_item_xfn', ''),
(824, 1703, '_menu_item_url', ''),
(970, 1016, '_edit_lock', '1540394608:1'),
(979, 1016, '_edit_last', '1'),
(981, 1016, 'sub_title', '811'),
(982, 1016, '_sub_title', 'field_1'),
(983, 1752, 'sub_title', '811'),
(984, 1752, '_sub_title', 'field_1'),
(987, 1016, 'image', '1687'),
(988, 1016, '_image', 'image'),
(989, 1016, 'caption', 'caption'),
(990, 1016, '_caption', 'caption'),
(991, 1016, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(992, 1016, '_description', 'description'),
(993, 1753, 'sub_title', '811'),
(994, 1753, '_sub_title', 'field_1'),
(995, 1753, 'image', '1687'),
(996, 1753, '_image', 'image'),
(997, 1753, 'caption', 'caption'),
(998, 1753, '_caption', 'caption'),
(999, 1753, 'description', 'texte de description'),
(1000, 1753, '_description', 'description'),
(1005, 1754, 'sub_title', '811'),
(1006, 1754, '_sub_title', 'field_1'),
(1007, 1754, 'image', '1687'),
(1008, 1754, '_image', 'image'),
(1009, 1754, 'caption', 'caption'),
(1010, 1754, '_caption', 'caption'),
(1011, 1754, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1012, 1754, '_description', 'description'),
(1079, 1016, '_wp_old_slug', 'template-featured-image-vertical'),
(1080, 1771, 'sub_title', '811'),
(1081, 1771, '_sub_title', 'field_1'),
(1082, 1771, 'image', '1687'),
(1083, 1771, '_image', 'image'),
(1084, 1771, 'caption', 'caption'),
(1085, 1771, '_caption', 'caption'),
(1086, 1771, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1087, 1771, '_description', 'description'),
(1106, 1773, '_menu_item_type', 'post_type'),
(1107, 1773, '_menu_item_menu_item_parent', '0'),
(1108, 1773, '_menu_item_object_id', '1016'),
(1109, 1773, '_menu_item_object', 'post'),
(1110, 1773, '_menu_item_target', ''),
(1111, 1773, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1112, 1773, '_menu_item_xfn', ''),
(1113, 1773, '_menu_item_url', ''),
(1130, 733, '_edit_lock', '1540392621:1'),
(1131, 733, '_edit_last', '1'),
(1133, 1778, 'sub_title', '811'),
(1134, 1778, '_sub_title', 'field_1'),
(1135, 1778, 'image', '1687'),
(1136, 1778, '_image', 'image'),
(1137, 1778, 'caption', 'caption'),
(1138, 1778, '_caption', 'caption'),
(1139, 1778, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1140, 1778, '_description', 'description'),
(1144, 733, 'image', '754'),
(1145, 733, '_image', 'image'),
(1146, 733, 'caption', 'caption'),
(1147, 733, '_caption', 'caption'),
(1148, 733, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'),
(1149, 733, '_description', 'description'),
(1150, 1779, 'image', '754'),
(1151, 1779, '_image', 'image'),
(1152, 1779, 'caption', 'caption'),
(1153, 1779, '_caption', 'caption'),
(1154, 1779, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'),
(1155, 1779, '_description', 'description'),
(1157, 1780, 'sub_title', '811'),
(1158, 1780, '_sub_title', 'field_1'),
(1159, 1780, 'image', '1687'),
(1160, 1780, '_image', 'image'),
(1161, 1780, 'caption', 'caption'),
(1162, 1780, '_caption', 'caption'),
(1163, 1780, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1164, 1780, '_description', 'description'),
(1166, 1781, 'sub_title', '811'),
(1167, 1781, '_sub_title', 'field_1'),
(1168, 1781, 'image', '1687'),
(1169, 1781, '_image', 'image'),
(1170, 1781, 'caption', 'caption'),
(1171, 1781, '_caption', 'caption'),
(1172, 1781, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1173, 1781, '_description', 'description'),
(1174, 1782, '_edit_last', '1'),
(1176, 1782, '_edit_lock', '1540394765:1'),
(1177, 1784, '_edit_last', '1'),
(1179, 1784, '_edit_lock', '1615037387:1'),
(1181, 1786, 'sub_title', '811'),
(1182, 1786, '_sub_title', 'field_1'),
(1183, 1786, 'image', '1687'),
(1184, 1786, '_image', 'image'),
(1185, 1786, 'caption', 'caption'),
(1186, 1786, '_caption', 'caption'),
(1187, 1786, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1188, 1786, '_description', 'description'),
(1190, 1787, 'sub_title', '811'),
(1191, 1787, '_sub_title', 'field_1'),
(1192, 1787, 'image', '1687'),
(1193, 1787, '_image', 'image'),
(1194, 1787, 'caption', 'caption'),
(1195, 1787, '_caption', 'caption'),
(1196, 1787, 'description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1197, 1787, '_description', 'description'),
(1200, 1791, '_wp_attached_file', '2021/03/logo.svg'),
(1201, 1791, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:138;s:6:\"height\";i:29;s:4:\"file\";s:17:\"/2021/03/logo.svg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:5:{s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";s:3:\"300\";s:6:\"height\";s:3:\"300\";s:4:\"crop\";b:0;s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";s:3:\"768\";s:6:\"height\";s:1:\"0\";s:4:\"crop\";b:0;s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:4:\"1024\";s:4:\"crop\";b:0;s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:30:\"twentyseventeen-featured-image\";a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:4:\"crop\";i:1;s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:5:{s:5:\"width\";i:100;s:6:\"height\";i:100;s:4:\"crop\";i:1;s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(1202, 1792, '_wp_trash_meta_status', 'publish'),
(1203, 1792, '_wp_trash_meta_time', '1615030133');

-- --------------------------------------------------------

--
-- Structure de la table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(733, 1, '2011-06-23 18:38:52', '2011-06-24 01:38:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Page A', '', 'publish', 'open', 'closed', '', 'page-a', '', '', '2018-10-24 16:50:36', '2018-10-24 14:50:36', '', 0, 'http://wpthemetestdata.wordpress.com/?page_id=733', 10, 'page', '', 0),
(754, 1, '2011-07-15 14:34:50', '2011-07-15 21:34:50', 'Public domain via http://www.burningwell.org/gallery2/v/Objects/100_5540.JPG.html', 'Bell on Wharf', 'Bell on wharf in San Francisco', 'inherit', 'open', 'closed', '', '100_5478', '', '', '2018-10-24 16:50:36', '2018-10-24 14:50:36', '', 733, 'http://192.168.99.100:8282/wp-content/uploads/2011/07/100_5478.jpg', 0, 'attachment', 'image/jpeg', 0),
(760, 1, '2011-07-15 14:41:41', '2011-07-15 21:41:41', 'Public domain via http://www.burningwell.org/gallery2/v/Objects/dsc09114.jpg.html', 'Sydney Harbor Bridge', 'Sydney Harbor Bridge', 'inherit', 'open', 'closed', '', 'dsc09114', '', '', '2011-07-15 14:41:41', '2011-07-15 21:41:41', '', 0, 'http://192.168.99.100:8282/wp-content/uploads/2011/07/dsc09114.jpg', 0, 'attachment', 'image/jpeg', 0),
(1016, 1, '2012-03-15 15:36:32', '2012-03-15 22:36:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 1', '', 'publish', 'closed', 'closed', '', 'article-1', '', '', '2018-10-24 17:25:51', '2018-10-24 15:25:51', '', 0, 'http://wptest.io/demo/?p=1016', 0, 'post', '', 0),
(1046, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Pages', '', 'publish', 'closed', 'closed', '', 'pages', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/pages/', 2, 'nav_menu_item', '', 0),
(1047, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Categories', '', 'publish', 'closed', 'closed', '', 'categories', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/categories/', 10, 'nav_menu_item', '', 0),
(1049, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', 'Posts in this category test post formats.', '', '', 'publish', 'closed', 'closed', '', '1049', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/', 24, 'nav_menu_item', '', 0),
(1051, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Depth', '', 'publish', 'closed', 'closed', '', 'depth', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/depth/', 29, 'nav_menu_item', '', 0),
(1052, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Level 01', '', 'publish', 'closed', 'closed', '', 'level-01', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-01/', 30, 'nav_menu_item', '', 0),
(1053, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Level 02', '', 'publish', 'closed', 'closed', '', 'level-02', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-02/', 31, 'nav_menu_item', '', 0),
(1054, 1, '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 'Level 03', '', 'publish', 'closed', 'closed', '', 'level-03', '', '', '2018-10-23 17:00:54', '2018-10-23 15:00:54', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-03/', 32, 'nav_menu_item', '', 0),
(1055, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 04', '', 'publish', 'closed', 'closed', '', 'level-04', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-04/', 33, 'nav_menu_item', '', 0),
(1056, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 05', '', 'publish', 'closed', 'closed', '', 'level-05', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-05/', 34, 'nav_menu_item', '', 0),
(1057, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 06', '', 'publish', 'closed', 'closed', '', 'level-06', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-06/', 35, 'nav_menu_item', '', 0),
(1058, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 07', '', 'publish', 'closed', 'closed', '', 'level-07', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-07/', 36, 'nav_menu_item', '', 0),
(1059, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 08', '', 'publish', 'closed', 'closed', '', 'level-08', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-08/', 37, 'nav_menu_item', '', 0),
(1060, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 09', '', 'publish', 'closed', 'closed', '', 'level-09', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-09/', 38, 'nav_menu_item', '', 0),
(1061, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Level 10', '', 'publish', 'closed', 'closed', '', 'level-10', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/level-10/', 39, 'nav_menu_item', '', 0),
(1062, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Advanced', '', 'publish', 'closed', 'closed', '', 'advanced', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/advanced/', 40, 'nav_menu_item', '', 0),
(1063, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', 'Custom Menu Description', 'Menu Description', '', 'publish', 'closed', 'closed', '', 'menu-description', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/menu-description/', 44, 'nav_menu_item', '', 0),
(1064, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Menu Title Attribute', 'Custom Title Attribute', 'publish', 'closed', 'closed', '', 'menu-title-attribute', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/menu-title-attribute/', 41, 'nav_menu_item', '', 0),
(1065, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'Menu CSS Class', '', 'publish', 'closed', 'closed', '', 'menu-css-class', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/menu-css-class/', 42, 'nav_menu_item', '', 0),
(1066, 1, '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 'New Window / Tab', '', 'publish', 'closed', 'closed', '', 'new-window-tab', '', '', '2018-10-23 17:00:55', '2018-10-23 15:00:55', '', 0, 'http://192.168.99.100:8282/2018/10/23/new-window-tab/', 43, 'nav_menu_item', '', 0),
(1629, 1, '2018-10-23 17:00:56', '2018-10-23 15:00:56', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-10-23 17:00:56', '2018-10-23 15:00:56', '', 0, 'http://192.168.99.100:8282/2018/10/23/home/', 1, 'nav_menu_item', '', 0),
(1703, 1, '2018-10-23 17:02:05', '2018-10-23 15:02:05', ' ', '', '', 'publish', 'closed', 'closed', '', '1703', '', '', '2018-10-24 16:44:44', '2018-10-24 14:44:44', '', 0, 'http://192.168.99.100:8282/2018/10/23/1703/', 2, 'nav_menu_item', '', 0),
(1752, 1, '2018-10-24 11:17:13', '2018-10-24 09:17:13', 'This post should display a <a title=\"Featured Images\" href=\"http://en.support.wordpress.com/featured-images/#setting-a-featured-image\" target=\"_blank\" rel=\"noopener\">featured image</a>, if the theme <a title=\"Post Thumbnails\" href=\"http://codex.wordpress.org/Post_Thumbnails\" target=\"_blank\" rel=\"noopener\">supports it</a>.\r\n\r\nNon-square images can provide some unique styling issues.\r\n\r\nThis post tests a vertical featured image.', 'Template: Featured Image (Vertical)', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 11:17:13', '2018-10-24 09:17:13', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1753, 1, '2018-10-24 14:05:47', '2018-10-24 12:05:47', 'This post should display a <a title=\"Featured Images\" href=\"http://en.support.wordpress.com/featured-images/#setting-a-featured-image\" target=\"_blank\" rel=\"noopener\">featured image</a>, if the theme <a title=\"Post Thumbnails\" href=\"http://codex.wordpress.org/Post_Thumbnails\" target=\"_blank\" rel=\"noopener\">supports it</a>.\r\n\r\nNon-square images can provide some unique styling issues.\r\n\r\nThis post tests a vertical featured image.', 'Template: Featured Image (Vertical)', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 14:05:47', '2018-10-24 12:05:47', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1754, 1, '2018-10-24 16:31:47', '2018-10-24 14:31:47', 'This post should display a <a title=\"Featured Images\" href=\"http://en.support.wordpress.com/featured-images/#setting-a-featured-image\" target=\"_blank\" rel=\"noopener\">featured image</a>, if the theme <a title=\"Post Thumbnails\" href=\"http://codex.wordpress.org/Post_Thumbnails\" target=\"_blank\" rel=\"noopener\">supports it</a>.\r\n\r\nNon-square images can provide some unique styling issues.\r\n\r\nThis post tests a vertical featured image.', 'Template: Featured Image (Vertical)', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 16:31:47', '2018-10-24 14:31:47', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1771, 1, '2018-10-24 16:43:02', '2018-10-24 14:43:02', 'This post should display a <a title=\"Featured Images\" href=\"http://en.support.wordpress.com/featured-images/#setting-a-featured-image\" target=\"_blank\" rel=\"noopener\">featured image</a>, if the theme <a title=\"Post Thumbnails\" href=\"http://codex.wordpress.org/Post_Thumbnails\" target=\"_blank\" rel=\"noopener\">supports it</a>.\r\n\r\nNon-square images can provide some unique styling issues.\r\n\r\nThis post tests a vertical featured image.', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 16:43:02', '2018-10-24 14:43:02', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1773, 1, '2018-10-24 16:44:44', '2018-10-24 14:44:44', ' ', '', '', 'publish', 'closed', 'closed', '', '1773', '', '', '2018-10-24 16:44:44', '2018-10-24 14:44:44', '', 0, 'http://192.168.99.100:8282/?p=1773', 4, 'nav_menu_item', '', 0),
(1777, 1, '2018-10-24 16:47:01', '2018-10-24 14:47:01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Page A', '', 'inherit', 'closed', 'closed', '', '733-revision-v1', '', '', '2018-10-24 16:47:01', '2018-10-24 14:47:01', '', 733, 'http://192.168.99.100:8282/2018/10/24/733-revision-v1/', 0, 'revision', '', 0),
(1778, 1, '2018-10-24 16:47:14', '2018-10-24 14:47:14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 16:47:14', '2018-10-24 14:47:14', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1779, 1, '2018-10-24 16:50:36', '2018-10-24 14:50:36', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Page A', '', 'inherit', 'closed', 'closed', '', '733-revision-v1', '', '', '2018-10-24 16:50:36', '2018-10-24 14:50:36', '', 733, 'http://192.168.99.100:8282/2018/10/24/733-revision-v1/', 0, 'revision', '', 0),
(1780, 1, '2018-10-24 17:19:38', '2018-10-24 15:19:38', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n<button>Get all posts</button>', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 17:19:38', '2018-10-24 15:19:38', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1781, 1, '2018-10-24 17:19:59', '2018-10-24 15:19:59', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n<button class=\"button-all-posts\">Get all posts</button>', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 17:19:59', '2018-10-24 15:19:59', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1782, 1, '2018-10-24 17:22:40', '2018-10-24 15:22:40', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 2', '', 'publish', 'open', 'open', '', 'article-2', '', '', '2018-10-24 17:26:04', '2018-10-24 15:26:04', '', 0, 'http://192.168.99.100:8282/?p=1782', 0, 'post', '', 0),
(1783, 1, '2018-10-24 17:22:40', '2018-10-24 15:22:40', '', 'Article 2', '', 'inherit', 'closed', 'closed', '', '1782-revision-v1', '', '', '2018-10-24 17:22:40', '2018-10-24 15:22:40', '', 1782, 'http://192.168.99.100:8282/2018/10/24/1782-revision-v1/', 0, 'revision', '', 0),
(1784, 1, '2018-10-24 17:22:50', '2018-10-24 15:22:50', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 3', '', 'publish', 'open', 'open', '', 'article-3', '', '', '2018-10-24 17:25:59', '2018-10-24 15:25:59', '', 0, 'http://192.168.99.100:8282/?p=1784', 0, 'post', '', 0),
(1785, 1, '2018-10-24 17:22:50', '2018-10-24 15:22:50', '', 'Article 3', '', 'inherit', 'closed', 'closed', '', '1784-revision-v1', '', '', '2018-10-24 17:22:50', '2018-10-24 15:22:50', '', 1784, 'http://192.168.99.100:8282/2018/10/24/1784-revision-v1/', 0, 'revision', '', 0),
(1786, 1, '2018-10-24 17:23:25', '2018-10-24 15:23:25', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n<button class=\"button-all-posts\">Show all posts</button>', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 17:23:25', '2018-10-24 15:23:25', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1787, 1, '2018-10-24 17:25:51', '2018-10-24 15:25:51', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 1', '', 'inherit', 'closed', 'closed', '', '1016-revision-v1', '', '', '2018-10-24 17:25:51', '2018-10-24 15:25:51', '', 1016, 'http://192.168.99.100:8282/2018/10/24/1016-revision-v1/', 0, 'revision', '', 0),
(1788, 1, '2018-10-24 17:25:59', '2018-10-24 15:25:59', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 3', '', 'inherit', 'closed', 'closed', '', '1784-revision-v1', '', '', '2018-10-24 17:25:59', '2018-10-24 15:25:59', '', 1784, 'http://192.168.99.100:8282/2018/10/24/1784-revision-v1/', 0, 'revision', '', 0),
(1789, 1, '2018-10-24 17:26:04', '2018-10-24 15:26:04', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Article 2', '', 'inherit', 'closed', 'closed', '', '1782-revision-v1', '', '', '2018-10-24 17:26:04', '2018-10-24 15:26:04', '', 1782, 'http://192.168.99.100:8282/2018/10/24/1782-revision-v1/', 0, 'revision', '', 0),
(1790, 1, '2021-03-06 12:19:40', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-03-06 12:19:40', '0000-00-00 00:00:00', '', 0, 'http://192.168.99.100:8282/?p=1790', 0, 'post', '', 0),
(1791, 1, '2021-03-06 12:28:17', '2021-03-06 11:28:17', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-03-06 12:28:17', '2021-03-06 11:28:17', '', 0, 'http://192.168.99.100:8282/wp-content/uploads/2021/03/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(1792, 1, '2021-03-06 12:28:53', '2021-03-06 11:28:53', '{\n    \"twentyseventeen::custom_logo\": {\n        \"value\": 1791,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-03-06 11:28:53\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1944248b-43ef-4a26-8f3c-a243bbc08603', '', '', '2021-03-06 12:28:53', '2021-03-06 11:28:53', '', 0, 'http://192.168.99.100:8282/2021/03/06/1944248b-43ef-4a26-8f3c-a243bbc08603/', 0, 'customize_changeset', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Non classé', 'non-classe', 0),
(177, 'All Pages', 'all-pages', 0),
(178, 'Short', 'short', 0),
(179, 'All Pages Flat', 'all-pages-flat', 0),
(180, 'Testing Menu', 'testing-menu', 0),
(181, 'Empty Menu', 'empty-menu', 0),
(182, 'Gallery', 'post-format-gallery', 0),
(183, 'Aside', 'post-format-aside', 0),
(184, 'Chat', 'post-format-chat', 0),
(185, 'Link', 'post-format-link', 0),
(186, 'Image', 'post-format-image', 0),
(187, 'Quote', 'post-format-quote', 0),
(188, 'Status', 'post-format-status', 0),
(189, 'Video', 'post-format-video', 0),
(190, 'Audio', 'post-format-audio', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1016, 1, 0),
(1046, 180, 0),
(1047, 180, 0),
(1049, 180, 0),
(1051, 180, 0),
(1052, 180, 0),
(1053, 180, 0),
(1054, 180, 0),
(1055, 180, 0),
(1056, 180, 0),
(1057, 180, 0),
(1058, 180, 0),
(1059, 180, 0),
(1060, 180, 0),
(1061, 180, 0),
(1062, 180, 0),
(1063, 180, 0),
(1064, 180, 0),
(1065, 180, 0),
(1066, 180, 0),
(1629, 178, 0),
(1703, 177, 0),
(1773, 177, 0),
(1782, 1, 0),
(1784, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(177, 177, 'nav_menu', '', 0, 2),
(178, 178, 'nav_menu', '', 0, 1),
(179, 179, 'nav_menu', '', 0, 0),
(180, 180, 'nav_menu', '', 0, 19),
(181, 181, 'nav_menu', '', 0, 0),
(182, 182, 'post_format', '', 0, 0),
(183, 183, 'post_format', '', 0, 0),
(184, 184, 'post_format', '', 0, 0),
(185, 185, 'post_format', '', 0, 0),
(186, 186, 'post_format', '', 0, 0),
(187, 187, 'post_format', '', 0, 0),
(188, 188, 'post_format', '', 0, 0),
(189, 189, 'post_format', '', 0, 0),
(190, 190, 'post_format', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'lengow'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '1790'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"192.168.99.0\";}'),
(19, 1, 'show_try_gutenberg_panel', '0'),
(20, 1, 'wp_user-settings', 'editor=html&hidetb=1&libraryContent=browse'),
(21, 1, 'wp_user-settings-time', '1540394374'),
(22, 1, 'nav_menu_recently_edited', '177'),
(23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(25, 1, 'closedpostboxes_post', 'a:0:{}'),
(26, 1, 'metaboxhidden_post', 'a:10:{i:0;s:9:\"formatdiv\";i:1;s:11:\"categorydiv\";i:2;s:16:\"tagsdiv-post_tag\";i:3;s:12:\"revisionsdiv\";i:4;s:11:\"postexcerpt\";i:5;s:13:\"trackbacksdiv\";i:6;s:16:\"commentstatusdiv\";i:7;s:11:\"commentsdiv\";i:8;s:7:\"slugdiv\";i:9;s:9:\"authordiv\";}'),
(27, 1, 'session_tokens', 'a:1:{s:64:\"90c2094c6caed18114ec0722b236bb8ebb20db6b84baaad490fa05c6e906a06b\";a:4:{s:10:\"expiration\";i:1615367675;s:2:\"ip\";s:12:\"192.168.99.1\";s:2:\"ua\";s:119:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15\";s:5:\"login\";i:1615194875;}}');

-- --------------------------------------------------------

--
-- Structure de la table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'lengow', '$P$BdRjbiTyB3fpvB4ZTgL79q0jAp6gzb.', 'lengow', 'marc.renou@lengow.com', '', '2018-10-23 12:27:32', '', 0, 'lengow');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Index pour la table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Index pour la table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Index pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Index pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Index pour la table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Index pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Index pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=562;

--
-- AUTO_INCREMENT pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1204;

--
-- AUTO_INCREMENT pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1793;

--
-- AUTO_INCREMENT pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
